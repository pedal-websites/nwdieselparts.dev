<?
	/**
	 * phpGenesis Config
	 *
	 * @author Jamon Holmgren and the ClearSight Team
	 * @link http://www.phpgenesis.com
	 *
	 */

	/**
	 * Check if site is on internal domain
	 */
	function dev() {
		return (strpos($_SERVER['HTTP_HOST'], "localhost") !== FALSE || strpos($_SERVER['HTTP_HOST'], ".dev") !== FALSE || strpos($_SERVER['HTTP_HOST'], ".site") !== FALSE); // checks the URL to see if ".int." is contained. You can check server IP, BASE_FOLDER, whatever.
	}


	/**
	 * Restricts IP addresses.
	 */
	$restrict_ip = false;	 // Set it to false to disable IP restriction
	$allowed_ips = array(
		"xxx.xxx.xxx.xxx",
	);
	if($restrict_ip && !in_array($_SERVER['REMOTE_ADDR'], $allowed_ips)) die("Coming soon!");

	/**
	 * Configure initial directories and URLs
	 */
	define("MAIN_FOLDER", dirname(__FILE__));	// Path to app and core folders here - no trailing slash
	define("BASE_FOLDER", MAIN_FOLDER);

	define('CORE_FOLDER', MAIN_FOLDER . "/_core_2.4.2"); // For csd_sites "/../dev.phpgenesis.com/_core_x.x.x"

	define("PLUGINS_FOLDER", MAIN_FOLDER . "/_plugins"); // If you don't have app-specific plugins use "/../dev.phpgenesis.com/_plugins
	define('APP_FOLDER', MAIN_FOLDER . "/app");
	define('UPLOADS_FOLDER', BASE_FOLDER . "/uploads");

	define('BASE_URL', "http://" . $_SERVER['HTTP_HOST']); // Do not use a trailing slash
	define('APP_URL', BASE_URL . "/app");
	define('APP_ID', "ncsJFKknorthwkjkjscescaastkngingmkJKjkeskkjsJLFVChjnyhknthikaandnRiifverkidaviiewnkknlGJhcichich");

	/**
	 * Define Application Environment
	 * Possible values: development, testing, production. maintenance
	 *
	 * development = strict, testing = warnings only, production = none, maintenance = redirects to a maintenance page
	 */
		if(dev()) {
			define('APP_STATUS', 'testing');
		} else {
			define('APP_STATUS', 'production');
		}

	/**
	 * Date/Time Settings
	 */
	date_default_timezone_set("America/Los_Angeles");

	/**
	 * WKHTML Configuration
	 */
	// define('WKHTMLTOPDF', 'xvfb-run -a -s "-screen 0 640x480x16" wkhtmltopdf --dpi 200');	// Use on Linux Servers
	// define('WKHTMLTOPDF', '\"c:\\Program Files\\wkhtmltopdf\\wkhtmltopdf\"'); 							// Use on Windows Servers

	/**
	 * Email
	 */
	define('TEST_EMAIL', "lucas@clearsightstudio.com");
	define('EMAIL', ((dev()) ? "lucas@clearsightstudio.com" : "requests@nwdieselparts.com"));
	// define('EMAIL', ((dev()) ? "avlux1@gmail.com" : "avlux1@gmail.com"));
	define('PHONE', "1 (503) 548-4125");
	define('SECONDARY_PHONE', "1 (503) 253-2500 (Int'l)");


	/**
	 * Application Specific Configuration
	 */
	function app_config() {
		// Application Version
		settings('app', 'version', "1.0");


		/**
		 * Preloaded Libraries
		 *
		 * (you can load them on the fly too using load_library("library_name"))
		 */
		settings('preload', 'libraries', array(
			'segments',
			'input',
			'form',
			'seo',
			'security',
			"layout",
			"activerecord",
			"users",
			"notice"
		));


		// Website Name
		settings("site", "name", "Northwest Diesel Parts");


		// Database Configuration db db_name value
		settings('db', 'enabled', true);								// db connection enabled (true|false)
		if(dev()) {
			settings('db', 'log_queries', true);						// log all queries
			settings('db', 'host', "127.0.0.1"); 						// localhost or mysql.example.com
			settings('db', 'username', "root");							// root or other
			settings('db', 'password', "root");		// password
			settings('db', 'database', "avlux_nwdieselparts");				// database name
		} else {
			settings('db', 'log_queries', false);						// log all queries
			settings('db', 'host', "localhost"); 						// localhost or mysql.example.com
			settings('db', 'username', "nwdiesel");							// root or other
			settings('db', 'password', "?gq6ic3G7Db");		// password
			settings('db', 'database', "avlux_nwdieselparts");				// database name
		}
		settings("activerecord", "models", APP_FOLDER . '/models/');


		// Default pages
		settings('pages', 'home_page', 'home');								// usually 'home'
		settings('pages', '404_page', '404');									// usually '404'
		settings('pages', 'access_denied', 'access-denied');	// usually 'access-denied'
		settings('pages', 'maintenance_page', 'maintenance');	// usually 'maintenance'


		// Input sanitizer - sanitizes data when input is requested
		// Available options: 'filter' (default), 'htmlpurifier' (requires thirdparty plugin), or 'simple' (not recommended)
		settings("input", "sanitizer", "filter");
		settings("input", "htmlsanitizer", "htmlpurifier");
		settings("input", "config", array(
			'HTML.Doctype' => 'HTML 4.01 Strict', // HTML 5 not supported yet?
			'Attr.EnableID' => true,
		));


		// form library
		settings("form", "submit_unchecked_checkboxes", true);		// checkboxes, if unchecked, will still submit if this is TRUE.


		// Cookies (load the "cookie" library to use)
		settings('cookie', 'path', '/');								// usually '/'
		settings('cookie', 'domain', '');								// usually ''
		settings('cookie', 'default_expire', 30);				// usually 30


		// Session (load the "session" library to use)
		settings('session', 'name', 'NORTHWESTDIESELPARTS');			// change this
		settings('session', 'timeout', 3600); 					// usually 3600
		// settings('session', 'save_path', BASE_FOLDER . "/../sessions"); // optional, untested


		// reCaptcha
		settings('recaptcha', 'publickey', '6Ld4JwgAAAAAAKLrtWx_mR4DqzioDTfW7ZBr9tka');
		settings('recaptcha', 'privatekey', '6Ld4JwgAAAAAAO2W5MC8VXkPuy7tUEhuY7f_h_O8');


		// Simple CMS
		settings('simplecms', 'table', 'simplecms_pages');	// database table that has the simplecms info


		// Simplelogin
		settings("simplelogin", "adminusers", array("username" => "password"));
		settings("simplelogin", "adminroot", "admin");
		settings("simplelogin", "adminlogin", "admin");
		settings("simplelogin", "adminhome", ADMIN_PATH . "/dashboard");
		settings("simplelogin", "adminusernamefield", "username"); // formname_fieldname (login_username)
		settings("simplelogin", "adminpasswordfield", "password"); // formname_fieldname (login_password)
		// load_library("simplelogin"); // Can also be put as a preloaded library


		// Users Library
		settings('users', 'login_type', 'email'); // email|username
		settings("users", "min_password_length", 8);
		settings('users', 'last_active_frequency', 1);
		settings('users', 'remember_me_timeout', 14);
		settings('users', 'trust_timeout', 300);
		settings('users', 'login_failure_limit', 3);
		settings('users', 'password_reset_timeout', 35);
		settings('users', 'allow_registration', true);
		settings("users", "activation_email_template", array(
			"from" => EMAIL,
			"subject" => "Welcome to our website! Please activate your account.",
			"message" => "
				<p>Welcome, %display_name%! We appreciate you taking the time to register on our website. Please click the following link or paste it into your browser to activate your account.</p>
				<p><a href='" . BASE_URL . "/users/activate/?activation_key=%activation_key%'>" . BASE_URL . "/users/activate/?activation_key=%activation_key%</a></p>
			",
		));
		settings("users", "reset_password_email_template", array(
			"from" => EMAIL,
			"subject" => "Password Reset",
			"message" => "
				<p>Your password is ready to be reset. Visit the link below to set a new password.</p>
				<p><a href='" . BASE_URL . "/users/reset/?reset_key=%reset_key%'>" . BASE_URL . "/users/reset/?reset_key=%reset_key%</a></p>
			",
		));
		// User levels. user_level("Manager") returns true if the user has a level of Admin or Manager
		settings('users', 'levels', array("Admin", "Manager", "User", "Limited"));


		// authorize.net settings
		settings("authorize", "login", "");
		settings("authorize", "transkey", "");
		settings("authorize", "test", true);

		settings('smtp', 'host', 'smtp.gmail.com');	// smtp.gmail.com
		settings('smtp', 'port', '465');						// 465 for ssl, 587 for tls
		settings('smtp', 'secure', 'ssl');					// ssl or tls
		settings('smtp', 'username', 'no-reply@nwdieselparts.com');	// username
		settings('smtp', 'password', 'aV=jK9=X');		// password
		settings('smtp', 'debug', true);						// debug true/false
		settings('smtp', 'from', 'no-reply@nwdieselparts.com'); // address to send from (if set phpmailer_send will use the from field as a reply-to address)

		// Notification Settings
		settings("notice", "html", "<div class='notice %type%'>%notice%</div>");
	}
?>
