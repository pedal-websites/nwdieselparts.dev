#!/usr/bin/env bash

#!/bin/sh
echo  'mysql-server mysql-server/root_password password root' | sudo debconf-set-selections #Cahnge the word 'vagrant' here to change the password.
echo  'mysql-server mysql-server/root_password_again password root' | sudo debconf-set-selections # Remember to do that here to!

sudo apt-get update

sudo apt-get install python-software-properties
sudo add-apt-repository ppa:ondrej/php5

sudo apt-get update

sudo apt-get -y install apache2 php5 mysql-server-5.5 php5-mysqlnd php5-xdebug

# see this page for more http://purencool.com/installing-xdebug-on-ubuntu
# update php.ini with zend info
echo 'zend_extension="/usr/lib/php5/20100525/xdebug.so"' >> /etc/php5/apache2/php.ini
echo 'xdebug.remote_enable=1' >> /etc/php5/apache2/php.ini
echo 'xdebug.remote_handler=dbgp' >> /etc/php5/apache2/php.ini
echo 'xdebug.remote_mode=req' >> /etc/php5/apache2/php.ini
echo 'xdebug.remote_host=10.0.2.2' >> /etc/php5/apache2/php.ini
echo 'xdebug.remote_port=9000' >> /etc/php5/apache2/php.ini
echo 'xdebug.idekey="IntelliJ"' >> /etc/php5/apache2/php.ini
echo 'xdebug.remote_autostart=1' >> /etc/php5/apache2/php.ini

mysql -u root -proot < /var/www/html/avlux_nwdieselparts.sql
mysql -u root -proot < /var/www/html/avlux_nwdieselparts_user.sql

sudo cp -rf /var/www/html/php.ini /etc/php5/apache2/php.ini
sudo cp -rf /var/www/html/000-default.conf /etc/apache2/sites-available

/etc/init.d/apache2 restart
