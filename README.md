Project setup:

Set up the db locally.

For running sass you will need -
  - Ruby 2.2.3
  - Run `bundle install`
  - To watch the main site styles, run: sass --watch ./app/style/application.scss:./app/style/application.css --sourcemap=none

  - To watch the admin styles: sass --watch ./app/style/admin.scss:./app/style/admin.css --sourcemap=none