<?
	function select_options($options, $sub_option_objects, $option_id, $option_label, $selected_id = 0, $level = 0) {
		$html = "";
		foreach($options as $option) {
			$selected = ""; if((int)$option->$option_id == (int)$selected_id) $selected = "selected='selected'";
			$space = str_repeat("&nbsp;&nbsp;&nbsp;", $level);
			$html .= "<option value='{$option->$option_id}' {$selected} {$style}>{$space}{$option->$option_label}</option>";

			if($option->$sub_option_objects) {
				$html .= select_options($option->$sub_option_objects, $sub_option_objects, $option_id, $option_label, $selected_id, $level + 1);
			}
		}
		return $html;
	}

	function featured_parts($category, $featured_parts = NULL) {
		/* Tries to get featured parts by category, doesn't work well
			if($category) {
				foreach($category->subcategories as $child) {
					if($child->subcategories) $featured_parts = featured_parts($child, $featured_parts);

					foreach($child->featured_parts as $cp) {
						$featured_parts[] = $cp;
					}
				}
				foreach($category->featured_parts as $part) {
					$featured_parts[] = $part;
				}
			} else {
				foreach(Part::find_all_by_featured("1", array("order" => "sort_order asc, updated_at desc", "limit" => 3)) as $part) {
					$featured_parts[] = $part;
				}
			}
		*/
		$featured_parts = Part::find_all_by_featured(1);
		shuffle($featured_parts);
		return $featured_parts;
	}
	function featured_parts_html($featured_parts, $limit = 3) {
		if(array_count($featured_parts) > 0) {
			$i = 0;
			foreach($featured_parts as $fp) {
				if($limit && $i < $limit) {
					$return .= featured_part($fp->id, TRUE);
					$return .= " ";
				}
				$i++;
			}
			return $return;
		}
	}

	function featured_part($id) {
		$part = Part::find($id);
		$category = Category::find($part->category_id);
		if($part->featured) {
			if(strlen($part->image_thumbname) > 1) {
				$a = $part->image_thumbname;
			} elseif(strlen($category->image_thumbname) > 1) {
				$a = $category->image_thumbname;
			} else {
				$a = "/app/media/images/no-image.jpg";
			}
			$html = "
				<div class='featured-part'>
					<h5><a href='/find-your-part/{$part->link}{$part->slug}/'>{$part->number}</a></h5>
					<p class='featured-description'>" . str_abbr($part->description, 35) . "</p>
					<a href='/find-your-part/{$part->link}{$part->slug}/'>
						<img src='{$a}' alt='{$part->number} - {$part->description}' />
					</a>
				</div>
			";
			return $html;
		}
		return "Not Found";
	}

	function parts_list($cat_id = NULL, $search = NULL, $limit = NULL) {
		if($search) {
			$parts = Part::search($search, $cat_id, $limit);
		} else {
			if($cat_id) {
				$cat = Category::find_by_id($cat_id);
				$parts = $cat->parts;
				// $parts = Part::category_parts($cat_id, array("order" => "sort_order asc, updated_at desc"));
			} else {
				$parts = Part::all(array("order" => "sort_order asc, updated_at desc", "limit" => $limit));
			}
		}
		$separator_count = 0;

		if(count($parts) == 1) {
			$part = $parts[0];
			notice_add("success", "Part found.");
			redirect("/find-your-part/" . $part->link . $part->slug . "/");
		} elseif(count($parts) > 0) {
			$return .= "<h3>Results From This Category</h3>";
			$counter = 0;
			foreach($parts as $index => $part) {
				if($cat_id AND $part->category_id != $cat_id AND $separator_count == 0) {
					$category = Category::find($cat_id);
					$return .= "<div class='cleared'></div>";
					$return .= "<h3>Results From Other Categories</h3>";
					$return .= "<div class='info-message'><p>These results for your search come from other categories than '{$category->name}.'</p></div>";
					$return .= "<div class='cleared'></div>";
					$separator_count += 1;
					$counter = 0;
				}

				$return .= _part($part);
				$counter += 1;
				if($counter % 3 == 0) $return .= "<div class='cleared'></div>";
			}
		} else {
			$return = "<p><strong>No results found.</strong></p>";
			$return .= "<p>Try another search or <a href='/contact/'>Contact Us</a> to get a custom quote.</p>";
		}
		return $return;
	}
	function _part($part) {
		$category = Category::find($part->category_id);

		$image_indicator = "<span class='image' title='Click here for an image'></span>"; // assume showing the image indicator

		if(strlen($part->image_thumbname) > 1) {
			$a = $part->image_thumbname;
		} elseif(strlen($category->image_thumbname) > 1) {
			$a = $category->image_thumbname;
		} else {
			$a = "/app/media/images/no-image.jpg";
			$image_indicator = ""; // if there is no image, don't show the image indicator
		}

		$html = "
			<div class='result'>
				<a href='/find-your-part/{$part->link}{$part->slug}/'>
					<img src='{$a}' alt='{$part->number} - {$part->description}'  />
				</a>
				<div class='info'>
					<h5><a href='/find-your-part/{$part->link}{$part->slug}/'>{$part->number} <!--{$image_indicator}--></a></h5>
					<p class='description'>" . nl2br(str_abbr($part->description, 70)) . "</p>
					<!--a href='/find-your-part/{$part->link}{$part->slug}/' class='grey-button'>View</a-->
				</div>
			</div>
		";
		return $html;
	}
?>
<? function image($float, $width, $file, $caption = NULL, $help = NULL) { ?>
	<div class="img-container <? if($float == "left") echo "img-left"; if($float == "right") echo "img-right"; ?>">
		<? if(file_exists(APP_FOLDER . "/media/images/" . $file . "-thumb.jpg")): ?>
			<a href="<?=APP_URL?>/media/images/<?=$file?>.jpg" class="lightbox" rel="lightbox-gallery"  title="<?=$caption?>" target="_blank">
				<img src="<?=APP_URL?>/media/images/<?=$file?>-thumb.jpg" width="<?=$width?>" alt="<?=$caption?>" />
			</a>
		<? else: ?>
			<img src="<?=APP_URL?>/media/images/<?=$file?>.jpg" width="<?=$width?>" alt="<?=$caption?>" />
		<? endif; ?>
		<? if($caption !== NULL): ?>
			<p class="caption">
				<? if($help !== NULL): ?>
					<?=$help?> -
				<? endif; ?>
				<?=$caption?>
			</p>
		<? endif; ?>
	</div>
<? } ?>