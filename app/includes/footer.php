		</div>
		<div class="cleared"></div>
		<div id="footer-container" class="container">
			<div id="footer" class="contained">
				<div class="third">
					<p><a href="/find-your-part/">Find Your Part</a></p>
					<p><a href="/remanufactured-long-block-engines/">Long Block Remanufacturing</a></p>
					<p><a href="/about/">About Us</a></p>
					<p><a href="/contact/">Contact Us</a></p>
				</div>
				<div class="third second">
					<? $parent_categories = Category::find_all_top_level(); ?>
					<? $cats = array_split($parent_categories); ?>
					<ul class="footer-menu submenu">
						<? if(is_array($cats[0])): ?>
							<? foreach($cats[0] as $c): ?>
								<li><a href="/find-your-part/<?=$c->slug?>/"><?=$c->name?></a></li>
							<? endforeach; ?>
						<? endif; ?>
					</ul>
					<ul class="footer-menu submenu">
						<? if(is_array($cats[1])): ?>
							<? foreach($cats[1] as $c): ?>
								<li><a href="/find-your-part/<?=$c->slug?>/"><?=$c->name?></a></li>
							<? endforeach; ?>
						<? endif; ?>
					</ul>
				</div>
				<div class="third last">
					<h3>Translate</h3>
					<div id="google_translate_element"></div>
					<script>
						function googleTranslateElementInit() {
							new google.translate.TranslateElement({
								pageLanguage: 'en',
								layout: google.translate.TranslateElement.InlineLayout.SIMPLE
							}, 'google_translate_element');
						}
					</script>
					<br />
					<script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
					<a href="#" onclick="return SnapABug.startLink();"><img src="https://snapabug.appspot.com/statusImage?w=215ea6d7-f3a3-4f56-b1e3-605476338db1" border="0"></a>
				</div>
				<p class="copyright">
					Copyright &copy; <?=date("Y")?> <?=settings("site", "name")?>. All rights reserved.<br />
					<a id="clearsightstudio-link" href="https://pedalwebsites.com/" title="Portland Oregon Web Design" target="_blank">Website Design by Pedal Websites.</a>
					<a href="https://plus.google.com/103402820244757499852" rel="publisher">Google+</a>
					<a href="//media/documents/Limited-Warranty-Policy-PDF.pdf" rel="publisher">Warranty</a>
					<a href="//media/documents/Terms-and-Conditions-of-Sale-PDF.pdf" rel="publisher">Terms of Service</a>
				</p>
			</div>
		</div>
		<script type="text/javascript">
			document.write(unescape("%3Cscript src='" + ((document.location.protocol=="https:")?"https://snapabug.appspot.com":"http://www.snapengage.com") + "/snapabug.js' type='text/javascript'%3E%3C/script%3E"));</script><script type="text/javascript">
			SnapABug.addButton("215ea6d7-f3a3-4f56-b1e3-605476338db1","1","40%");
		</script>
		<? foot_hook(); ?>
		<!-- denton -->
	</body>
</html>