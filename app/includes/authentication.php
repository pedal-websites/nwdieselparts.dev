<?
	load_libraries(array("users", "form", "notice"));
	
	if(globals("users", "authentication_loaded")) {
		$b = debug_backtrace();
		echo "<pre>Authentication include loaded twice.\n\n\nTrying to load it in {$b[1]['file']}. Already loaded in " . globals("users", "authentication_loaded") . "\n\n\n\n";
		die();
	} else {
		$b = debug_backtrace();
		globals("users", "authentication_loaded", $b[1]['file']);
	}
	
	/*************************************** Logs out ***********************************/
	if(input_get("logout") == "true") {
		user_logout();
		notice_add("info", "Logged out!", "login");
		redirect("/admin/users/login/");
	}
	
	/*************************************** Handles registration ************************/
	if(form_posted("register", FALSE)) {
		if(settings("users", "allow_registration")) {
			$display_name = form_validate("display_name");
			$email = form_validate("email");
			if(settings('users', 'login_type') == "username") {
				$username = form_validate("username");
			} else {
				$username = $email;
			}
			$password = form_validate("password");
			$conf_pass = form_validate("conf_pass");
			
			if(form_is_valid()) {
				list($user_is_valid, $message) = user_test($username, $email, $password, $conf_pass);
				if($user_is_valid) {
					$key = user_register($username, $email, $password, $display_name);
					if($key) {
						user_send_activation($key, $email, array("display_name" => $display_name, "email" => $email, "username" => $username));
						notice_add("success", "You have successfully registered! Check your email for the activation link.", "login");
					} else {
						notice_add("error", "Couldn't register user!", "login");
					}
				} else {
					notice_add("error", $message, "login");
				}
			} else {
				notice_add("error", "Couldn't register -- check your entries and try again.", "login");
			}
		} else {
			notice_add("error", "Registration not enabled for this website.", "login");
		}
	}
	
	/*************************************** Handles activation ************************/
	if(input_get("activation_key") !== NULL) {
		$key = input_get("activation_key");
		if($activated_user = user_activate_and_log_in($key)) {
			notice_add("success", "User activated!", "login");
		} else {
			notice_add("error", "Sorry, activation failed. Please try again.", "login");
		}
	}
	
	/*************************************** Handles login ************************/
	if(form_posted("login", FALSE)) {
		$username = form_validate("username", "trim|val_required");
		$password = form_validate("password");
		$remember = form_validate_checkbox("remember");
		
		if(form_is_valid()) {
			if(user_login($username, $password, $remember)) {
				notice_add("info", "Successfully logged in! Welcome!", "login");
			} else {
				notice_add("error", "Invalid username and/or password.", "login");
			}
		} else {
			notice_add("error", "You forgot to fill out one of the fields!", "login");
		}
	}
	
	/*************************************** Handles forgot password *************************/
	if(form_posted("forgot_password", FALSE)) {
		$email = form_validate("email", "trim|val_email|val_required");
		
		if(form_is_valid() && user_get_id_by_other($email) > 0) {
			user_send_reset_password($email);
			notice_add("success", "Please check your email for the reset password link.", "login");
		} else {
			notice_add("error", "Couldn't reset password - check your email address.", "login");
		}
	}
	if(form_posted("reset_password", FALSE)) {
		$reset_key = form_validate("reset_key", "trim|val_required");
		$new_password = form_validate("password", "trim|val_required");
		$conf_pass = form_validate("conf_pass", "trim|val_required");
		
		if(form_is_valid() && $new_password == $conf_pass) {
			if($updated_user = user_reset_password($reset_key, $new_password)) {
				notice_add("success", "We have successfully reset your password.", "login");
				
				$user_type = "username";
				if(setting_isset('users', 'login_type')) $user_type = settings('users', 'login_type');
				
				user_login($updated_user[$user_type], $new_password, FALSE);
			} else {
				notice_add("error", "Couldn't reset your password! Please try again.", "login");
			}
		} else {
			notice_add("error", "Couldn't reset password - make sure your passwords match.", "login");
		}
	}
	
?>