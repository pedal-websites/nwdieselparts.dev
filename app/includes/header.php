<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><?=meta('title')?></title>
		<meta name="keywords" content="<?=meta('keywords')?>" />
		<meta name="description" content="<?=meta('description')?>" />
		<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
		<link rel="apple-touch-icon" href="/apple-touch-icon.png">
		<link rel="shortcut icon" href="/favicon.ico">
		<?=canonical()?>
		<? register_css("slimbox", "http://api.devcsd.com/js/slimbox/css/slimbox2.css", "all", 1); ?>
		<? register_css("application", "style/application.css", "all", 2); ?>
		<? register_css("style-ie", "style/ie.css", "all", 3, "ie"); ?>
		<? register_css("style-ie6", "style/ie6.css", "all", 4, "lt ie 7"); ?>

		<? register_javascript("jquery", "http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js", 1); ?>
		<? register_javascript("slimbox", "http://api.devcsd.com/js/slimbox/js/slimbox2.js", 15); ?>
		<? register_javascript("header", "javascript/header.js", 16); ?>
		<? register_javascript("onload", "javascript/onload.js", 9999); ?>
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-24258568-1']);
			_gaq.push(['_trackPageview']);
			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
		</script>
		<? head_hook(); ?>
		<? if(!cookie("VISITOR_REFERRER")) cookie("VISITOR_REFERRER", $_SERVER['HTTP_REFERER'], 30); ?>
	</head>
	<? $body_id = (meta('body_id') <> "") ? meta('body_id') : str_replace("/", "-",  segments_full()); $body_class = (meta('body_class') <> "") ? meta('body_class') : str_replace("/", "-",  segments_page()); ?>
	<body id="body-<?=$body_id?>" class="body-<?=$body_class?>">
		<div id="wrapper">
			<?php //Just keeping around the old header, in case things don't go well.
            if(false) {?>
            <div id="header-container" class="container">
				<div id="header" class="contained">
					<a class="logo" href="/" title="Northwest Diesel Parts" id="logo">
						<img class="logo" src="<?=APP_URL?>/media/images/nw-diesel-parts-logo.jpg" width="100%" />
					</a>
					<div id="top" class="cleared">
						<?php
							$cart_items = 0;
							if(cookie_isset("cart_id")) {
								$cart = QuoteRequest::find_by_id(cookie("cart_id"));
								$cart_items = count($cart->quote_request_parts);
							}
						?>
						<a id="cart" href="/cart/"><span class="strong-span"><?=$cart_items?></span><span class="inline-span"> items in your quote </span><br />&nbsp;&nbsp;&nbsp;<span class="tiny-text">*No sales tax collected</span></a>
						<div class="text"><?=PHONE?></div>
						<div class="mobile-intlnumber">+<?=SECONDARY_PHONE?></div>
						<div id="hamburger"><img src="<?=APP_URL?>/media/images/hamburger-icon.png" width="50px"></div>
						<form id="header-search" class="non-mobile" enctype="multipart/form-data" method="get" action="/find-your-part/">
							<input type="text" name="search" placeholder="Search, then press enter" />
						</form>
						<div class="intlnumber"><br />+<?=SECONDARY_PHONE?></div>
						<div id="menu" class="cleared">
                            <a href="/find-your-part/" class="first <?=(segments_page() == "find-your-part") ? "current" : ""; ?>">Product Line</a>
                            <a href="/remanufactured-long-block-engines/" class="<?=(segments_page() == "remanufactured-long-block-engines") ? "current" : ""; ?>">Remanufactured Long Blocks</a>
                            <a href="/about/" class="<?=(segments_page() == "about") ? "current" : ""; ?>">About Us</a>
                            <a href="/contact/" class="<?=(segments_page() == "contact") ? "current" : ""; ?>">Contact Us</a>
						</div>

					</div>
				</div>
			</div>
        <?php } ?>
			<div id="header-container" class="container">
				<div id="header" class="contained">
					<a style="" class="logo" href="/" title="Northwest Diesel Parts" id="logo">
						<img src="https://i.imgur.com/FyJMteu.gif" class="logo" width="100%">
					</a>
					<div id="top" class="cleared">
                        <?php
                        $cart_items = 0;
                        if(cookie_isset("cart_id")) {
                            $cart = QuoteRequest::find_by_id(cookie("cart_id"));
                            $cart_items = count($cart->quote_request_parts);
                        }
                        ?>
						<div id="upper-items">
                            <a id="cart" href="/cart/">
                                <div class="strong-span"><?=$cart_items?></div>
                                <div class="cart-text">
                                    <div class="inline-span"> items in your quote </div>
                                    <div class="tiny-text">*No sales tax collected</div>
                                </div>
                            </a>
							<div class="phone-area">
                                <div><?=PHONE?></div>
								<div>+<?=SECONDARY_PHONE?></div>
                            </div>
							<div id="hamburger">
                                <img src="<?=APP_URL?>/media/images/hamburger-icon.png" width="50px">
                            </div>
							<form id="header-search" class="non-mobile" enctype="multipart/form-data" method="get" action="/find-your-part/">
								<input name="search" placeholder="Search, then press enter" type="text">
							</form>
						</div>
						<div class="menu-items">
                            <a href="/find-your-part/" class="menu-item first <?=(segments_page() == "find-your-part") ? "current" : ""; ?>">Product Line</a>
                            <a href="/about/" class="menu-item <?=(segments_page() == "about") ? "current" : ""; ?>">About Us</a>
                            <a href="/contact/" class="menu-item <?=(segments_page() == "contact") ? "current" : ""; ?>">Contact Us</a>
							<a href="/remanufacturing-process/" class="menu-item <?=(segments_page() == "remanufacturing-process") ? "current" : ""; ?>">Remanufacturing Process</a>
                        </div>
                    </div>
				</div>
			</div>








			<div class="mobile-menu" id="mobile-menu" class="cleared">
				<img src="<?=APP_URL?>/media/images/dot.png" width="9px">
				<a href="/find-your-part/" class="first <?=(segments_page() == "find-your-part") ? "current" : ""; ?>">Find Your Part</a>
				<img src="<?=APP_URL?>/media/images/dot.png" width="9px">
				<a href="/remanufactured-long-block-engines/" class="<?=(segments_page() == "remanufactured-long-block-engines") ? "current" : ""; ?>">Remanufactured Long Blocks</a>
				<img src="<?=APP_URL?>/media/images/dot.png" width="9px">
				<a href="/about/" class="<?=(segments_page() == "about") ? "current" : ""; ?>">About Us</a>
				<img src="<?=APP_URL?>/media/images/dot.png" width="9px">
				<a href="/contact/" class="<?=(segments_page() == "contact") ? "current" : ""; ?>">Contact Us</a>
				<img src="<?=APP_URL?>/media/images/dot.png" width="9px">
				<form id="header-search" enctype="multipart/form-data" method="get" action="/find-your-part/">
					<input type="text" name="search" placeholder="Search, then press enter" />
				</form>
			</div>