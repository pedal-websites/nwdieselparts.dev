<?
	if((!user_logged_in() || !user_is_admin())) {
		if(strpos(segments_full(), "admin/users") === FALSE) {
			redirect("/admin/users/login");
		}
	}
?>
<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><?=meta('title')?></title>
		<meta name="keywords" content="<?=meta('keywords')?>" />
		<meta name="description" content="<?=meta('description')?>" />
		<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
		<link rel="apple-touch-icon" href="/apple-touch-icon.png">
		<link rel="shortcut icon" href="/favicon.ico">
		<?=canonical()?>
		<? if(!dev()): ?>
		<? endif; ?>

		<? register_css("admin", "style/admin.css", "all", 1); ?>
		<? register_css("jquery-ui", "http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/redmond/jquery-ui.css", "all", 4); ?>

		<? register_javascript("jquery", "http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js", 1); ?>
		<? register_javascript("jquery-migrate", "http://code.jquery.com/jquery-migrate-1.1.1.js", 2); ?>
		<? register_javascript("jquery-hashchange", "javascript/plugins/jquery.hashchange.js", 3); ?>
		<? register_javascript("jquery-tmpl", "javascript/plugins/jquery.tmpl.min.js", 4); ?>
		<? register_javascript("jquery-ui", "http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js", 5); ?>
		<? register_javascript("onload", "javascript/onload.js", 9998); ?>
		<? register_javascript("admin-onload", "javascript/admin.js", 9999); ?>

		<? head_hook(); ?>
	</head>
	<body>
		<div id="top-container" class="container">
			<? if(dev() && strpos($_SERVER['REMOTE_ADDR'], "192.168.3") === false): ?>
				<div id="notice-top">
					<p>This is the DEV site.  Edits will not be apparent on the live site! Please report bugs to <a href="mailto:programmers@clearsightstudio.com">programmers@clearsightstudio.com</a></p>
				</div>
			<? endif; ?>

			<div id="top" class="contained">
				<h1>NW Diesel Admin</h1> <a href="<?=base_url()?>" id="view-site" target="_blank">view site</a>
				<div id="links">
					<? if(user_logged_in()): ?>
						<a href="/admin/users/<?=user_id()?>/edit/"><?=user_display_name();?></a>
						<a href="/admin/users/?logout=true">Log out</a>
					<? endif; ?>
				</div>
				<? if(user_logged_in()): ?>
					<div id="menu">
						<a href="/admin/" class="<? if(segments_full(1) == "") echo "current"; ?>">Dashboard</a>
						<a href="/admin/quote-requests/" class="<? if(segments_full(1) == "quote-requests") echo "current"; ?>">Quote Requests</a>
						<a href="/admin/product-catalog" class="<? if(segments_full(1) == "product-catalog") echo "current"; ?>">Product Catalog</a>
						<a href="/admin/manufacturers/" class="<? if(segments_full(1) == "manufacturers") echo "current"; ?>">Manufacturers</a>
						<a href="/admin/videos/" class="<? if(segments_full(1) == "videos") echo "current"; ?>">Videos</a>
						<a href="/admin/users" class="<? if(segments_full(1) == "users") echo "current"; ?>">Users</a>
					</div>
				<? endif; ?>
			</div>
		</div>
		<div id="main-container" class="container">
			<? if(!strpos(segments_full(), "product-catalog") > 0) : ?>
				<? // don't show this on the product catalog page.  Breadcrumbs redundant ?>
				<div class="breadcrumbs">
					<?=segments_breadcrumbs("&raquo;", FALSE)?>
				</div>
			<? else: ?>
				<?=layout_section("subnav"); ?>
			<? endif; ?>
			<div id="main" class="contained">
				<?=layout_section("main")?>
			</div>
		</div>
		<? foot_hook(); ?>
	</body>
</html>