<? app_include("header"); ?>
<div id="mid-container" class="container">
	<div id="mid" class="contained">
		<div id="page-title" class="cleared"><?=layout_section("page-title")?></div>
		<? if(layout_section("sidebar")): ?>
			<div id="sidebar">
				<?=layout_section("sidebar")?>
			</div>
		<? endif; ?>
		<? if(layout_section("content")): ?>
			<div id="content" class="cleared">
				<?=layout_section("content")?>
			</div>
		<? endif; ?>
		<? if(layout_section("full-width")): ?>
			<div id="full-width" class="cleared">
				<?=layout_section("full-width")?>
			</div>
		<? endif; ?>
	</div>
</div>
<? app_include("footer"); ?>