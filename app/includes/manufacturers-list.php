<? /*
<ul>
	<li>Caterpillar</li>
	<li>Cummins</li>
	<li>Detroit</li>
	<li>International</li>
	<li>Mack</li>
	<li>John Deere</li>
	<li>Komatsu</li>
	<li>Navistar</li>
	<li>Kubota</li>
	<li>Perkins</li>
	<li>Waukesha</li>
	<li>White Superior</li>
	<li>Bessemer</li>
	<li>Case</li>
</ul>
*/ ?>
<ul>
	<? $manufacturers = Manufacturer::all(); ?>
	<? if(count($manufacturers) > 0): ?>
		<? foreach($manufacturers as $m): ?>
			<li><?=$m->name?></li>
		<? endforeach; ?>
	<? endif; ?>
</ul>