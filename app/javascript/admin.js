/**
 * Admin jQuery
 *
 * Please keep organized!
 */
 
//----
// Quote Requests Feed
//----
var $quoteFeed = $("#quote-request-feed");
var $modal = $("#request_modal");

$("table.sortable").sortable({
	handle : ".handle",
	items : "tr",
	placeholder : "ui-state-highlight",
	stop : function(event, ui){
		var ordered_items = $(this).sortable("serialize");
		$.getJSON("/admin/product-catalog/ajax/sort/", ordered_items, function(response){
			// callback stuff here
		})
	}
});


if($quoteFeed.length > 0) {
	// Create refresh/loading script
	$quoteFeed.bind("refresh", function() {
		$quoteFeed.addClass("loading");
		
		// Define Paramaters here
		var status = "submitted";
		var pageUrl = document.location.toString();
		if (pageUrl.match('#')) status = pageUrl.split('#')[1];
		
		// Update status menu
		$("#submenu a").removeClass("current");
		$currentStatusLink = $("#submenu a[href=#" + status + "]").addClass("current");
		
		// Load quote requests into page
		$quoteFeed.load("/admin/quote-requests/ajax/feed/", {"status" : status}, function() {
		});
		
		$quoteFeed.removeClass("loading");
	});
	
	// Load feed initially
	$quoteFeed.trigger("refresh");
	
	// Refresh feed on hash change
	$(window).hashchange(function() {
		$quoteFeed.trigger("refresh");																
	});
	
	// Refresh ever 30 seconds
	function refresh_quote_feed() {
		$quoteFeed.trigger("refresh");
	}
	var automaticQuoteFeedUpdate = setInterval("refresh_quote_feed()", 30000);
	
	// Modal Window
	$quoteFeed.delegate(".request", "click", function() {
		var $request_row = $(this);
		var request = $.parseJSON($request_row.attr("data-request"));		
		var $parts_list = $modal.find(".parts");
		var $referrer = $modal.find(".referrer");

		// Reset Modal
		$modal.find("form input").val("");
		$parts_list.html("");
		$referrer.attr("href", "javascript:;");
		$modal.data("requst_id", request.id);
		
		// Generate Data and Insert
		$("#email_customer_email_to").val(request.email);
		$(".current-status").html(request.status);
		$(".name").html(request.name);
		$(".phone").html(request.phone);
		$(".email").html(request.email);
		$(".feedback").html(request.feedback);
		$referrer.attr("href", request.referrer);

		$.each(request.parts, function(index, part) {
			var $newPart = $("<li></li>");
			var $questions = $("<ul></ul>").addClass("questions");

			// Questions
			$.each(part.question_answers, function(index, question) {
				$q_html = $("<li></li>").html(question.question + " - <strong>" + question.answer + "</strong>");
				$questions.append($q_html);
			});
			
			var link_text = "<a href='/admin/product-catalog/parts/" + part.part_id + "/edit/' target='_blank'>" + part.number + "</a>";
			$newPart.html(link_text + " - " + part.description);
			$newPart.append($questions);
			
			
			$parts_list.append($newPart);
		});
		
		// Change Quote Request Status
		$modal.find(".statuses button").unbind("click").click(function(){
			var status = $(this).val();
			$modal.addClass("loading");
			$.get("/admin/quote-requests/ajax/change-status/" + request.id, {"status" : status}, function() {
				$(".current-status").html(status);		
				$quoteFeed.trigger("refresh");
				$modal.removeClass("loading");
			});
		});
		
		// Load Modal
		poplight($(this));
	});
	
	
}

//----
// Basic Stuff
//----

$(".delete, .confirm").click(function() {
	$link = $(this);
	if($link.data("confirm")) {
		var text = $link.data("confirm");
	} else {
		var text = "Are you sure you want to do this?";
	}
	return confirm(text);
});

//----
// Category Questions
//----
$questions = $("#questions");
// Add Question
$("#add-question").click(function(event) {
	$newQuestion = $(".question:eq(0)").clone();
	$newQuestion.find("input, select, textarea").val("");
	$questions.prepend($newQuestion);
	event.preventDefault();
});

// Delete question
$questions.delegate(".question .delete", "click", function(event) {
	$(this).closest(".question").remove();
	event.preventDefault();
});

// Question Type Change Action
$questions.delegate(".question select", "change", function() {
	if($(this).val() == 1) {
		$(this).closest(".question").find(".options").removeAttr("readonly").removeClass("readonly");
	} else {
		$(this).closest(".question").find(".options").attr("readonly", "readonly").addClass("readonly");
	}
});

// Sort Questions
var sortableSettings = {
	items : '.question',
	tolerance : 'intersect',
	placeholder : 'ui-state-highlight'
}
$questions.sortable(sortableSettings);