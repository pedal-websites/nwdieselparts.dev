function poplight($link) {
	var popID = $link.attr('rel'); //Get Popup Name
	var popURL = $link.attr('href'); //Get Popup href to define size
//	console.log("test");
	//Pull Query & Variables from href URL
	var query= popURL.split('?');
	var dim= query[1].split('&');
	var popWidth = dim[0].split('=')[1]; //Gets the first query string value

	//Fade in the Popup and add close button
	$('#' + popID).fadeIn().css({ 'width': Number( popWidth ) }).prepend('<a href="#" class="close"><img src="/app/media/images/close_pop.png" class="btn_close" title="Close Window" alt="Close" /></a>');

	//Define margin for center alignment (vertical   horizontal) - we add 80px to the height/width to accomodate for the padding  and border width defined in the css
	var popMargTop = ($('#' + popID).height() + 80) / 2;
	var popMargLeft = ($('#' + popID).width() + 80) / 2;

	//Apply Margin to Popup
	$('#' + popID).css({
			'margin-top' : -popMargTop,
			'margin-left' : -popMargLeft
	});

	//Fade in Background
	$('body').append('<div id="fade"></div>'); //Add the fade layer to bottom of the body tag.
	$('#fade').css({'filter' : 'alpha(opacity=80)'}).fadeIn(); //Fade in the fade layer - .css({'filter' : 'alpha(opacity=80)'}) is used to fix the IE Bug on fading transparencies

	return false;
}

function isValidEmailAddress(emailAddress) {
	var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
	return pattern.test(emailAddress);
};

(function($){

	// jQuery code here
	$(".add-to-cart").click(function(e) {
		e.preventDefault();
		$partQuestions = $("#part_questions");
		if($partQuestions.find(".no-questions").length > 0) {
			$partQuestions.find("form").trigger("submit");
		} else {
			poplight($(this));
		}
	});
	$(".expand-video").click(function() {
		poplight($(this));
	});

	//When you click on a link with class of poplight and the href starts with a #
	$('a.poplight[href^=#]').click(function() {poplight($(this));});

	//Close Popups and Fade Layer
	$('a.close, #fade').live('click', function() { //When clicking on the close or fade layer...
			$('#fade , .popup_block').fadeOut(function() {
					$('#fade, a.close').remove();  //fade them both out
			});
			return false;
	});

	$(".confirm").live("click", function() {
		return confirm($(this).attr("data-confirm"));
	});

	$("#submit_cart").live("submit", function() {
		var $form = $(this);
		var $name = $form.find("#submit_cart_name");
		var $email = $form.find("#submit_cart_email");
		var $phone = $form.find("#submit_cart_phone");

		var error = false;
		var message = "";
		if($name.val().length < 2) {
			error = true;
			message = message + "You need to fill out your name. ";
		}
		if($email.val().length < 4 || !isValidEmailAddress($email.val())) {
			error = true;
			message = message + "You need to fill out a valid email address. ";
		}
		if($phone.val().length < 7) {
			error = true;
			message = message + "You need to fill out your phone. ";
		}

		if(error) {
			alert(message);
			return false;
		}
	});



})(window.jQuery);



window.log = function(){
  log.history = log.history || [];
  log.history.push(arguments);
  if(this.console){
    console.log( Array.prototype.slice.call(arguments) );
  }
};

jQuery.fn.exists = function(){return jQuery(this).length>0;} // Usage: if($(".element").exists()) { /* do something */ }