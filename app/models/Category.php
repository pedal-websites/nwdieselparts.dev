<?php
	class Category extends AppModel {
		static $before_save = array("apply_slug");
		static $after_save = array("after_save");
		static $has_many = array(
			array("parts", "order" => "sort_order ASC, updated_at DESC"),
			array("featured_parts", "class_name" => "Part", "conditions" => "featured = 1", "order" => "updated_at desc"),
			array("subcategories", "class_name" => "Category", "foreign_key" => "parent_id", "order" => "name"),
			array("questions")
		);
		
		static $belongs_to = array(
			array("parent_category", "class_name" => "Category", "foreign_key" => "parent_id"),
		);
		
		private $parents, $parent_ids, $children, $child_ids; // arrays of all my parents and my parent ids
		private $parts_count;
		
		public function apply_slug() {
			if($this->parent_category) {
				$this->slug = $this->parent_category->slug . "/" . slugify($this->name);
			} else {
				$this->slug = slugify($this->name);
			}
		}
		
		static public function save_all() {
			foreach(Category::all() as $c) {
				$c->apply_slug();
				$c->save();
			}
		}
		
		public function update_parts() {
			self::query("
				UPDATE parts
				SET link = '{$this->slug}/'
				WHERE category_id = {$this->id}
			");
			foreach($this->subcategories as $s) {
				$s->slug = ""; // reset it so it will save
				$s->save(); // updates the slug (with my new updated slug) and all parts
			}
		}
		
		public function before_save() {
			$this->apply_slug();
		}
		
		public function after_save() {	
			$this->update_parts();
		}
		
		/**
		 *	Use $category->validate_parents() to see if it's valid and $category->errors->on("parent_id") to display the error.
		 *	
		 */
		public function validate_parents() {
			// Check to make sure there is no looped categories. Otherwise, no saving is allowed.
			if($this->parent_id == $this->id && $this->parent_id != NULL) {
				notice_add("error", "You can't set a category's parent to itself.");
				$this->parent_id = NULL;
				return FALSE;
			}
			if($this->parents() === FALSE) {
				notice_add("error", "Couldn't save parent category. Looped category found.");
				$this->parent_id = NULL;
				return FALSE;
			}
			return TRUE;
		}
		
		/**
		 *	Returns recursive parent array with ids.
		 *
		 *	@return array
		 */
		public function parent_ids() {
			$this->parents();
			return $this->parent_ids;
		}
		
		/**
		 *	Returns recursive parent array with ids as keys. 
		 *	Will recognize if there is a loop and return FALSE 
		 *	if so. If there are none will just return an empty array.
		 *
		 *	@return array
		 */
		public function parents() {
			if(!$this->parents && $this->parent_id) {
				$temp_category = $this;
				while($temp_category = self::find($temp_category->parent_id)) {
					if(isset($this->parents[$temp_category->id])) {
						$this->parents = NULL;
						$this->parent_ids = NULL;
						return FALSE; // error!
					}
					
					$this->parents[$temp_category->id] = $temp_category;
					$this->parent_ids[] = $temp_category->id;
				}
			}
			if(!$this->parents) $this->parents = array();
			
			return $this->parents;
		}
		
		public function children() {
			return $this->all_subcategories();
		}
		
		private function all_subcategories($sub_categories = array()) {
			foreach($this->subcategories as $sub) {
				$sub_categories[] = $sub;
				if(count($sub->subcategories) > 0) $sub_categories = $sub->all_subcategories($sub_categories);
			}
			return $sub_categories;
		}
		
		
		public function parts_count() {
			if($this->id && !$this->parts_count) {
				$this->parts_count = array_first(self::query("
					SELECT COUNT(id) FROM parts WHERE category_id = " . $this->id . "
				")->fetch());
			}
			return $this->parts_count;
		}
		
		public function parts() {
			$parts = Parts::find_by_category_id($this->id);
			return $parts;
		}
		
		/* Not implemented because it's not needed
			public function all_parts() {
				
			}
		*/
		
		/**
		 *	Returns all top level categories.
		 *
		 *	@return array
		 */
		public static function find_all_top_level() {
			return self::find_all_by_parent_id(NULL, array("order" => "name ASC"));
		}
		
	}
?>