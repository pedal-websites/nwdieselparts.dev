<?php
	class Part extends AppModel {
		static $belongs_to = array(
			array("category"),
			array("manufacturer")
		);
		static $before_save = array("update_link", "update_keywords", "update_special_search");

		static public function search($search, $category_id = NULL, $limit = 100) {

				$words = explode(" ", $search);
				$conditions = "(number LIKE \"%" . $search . "%\"";
				foreach($words as $w) {
					$word_conditions .= "(special_search LIKE \"%" . $w . "%\" OR alternate_numbers LIKE \"%" . $w . "%\") AND ";
					// $word_conditions .= "(link LIKE \"%" . $w . "%\"" . " OR alternate_numbers LIKE \"%" . $w . "%\"" . " OR description LIKE \"%" . $w . "%\" OR keywords LIKE \"%" . $w . "%\") AND ";
				}
				$word_conditions = " OR (" . rtrim($word_conditions, " AND "). ")";

				$conditions .= $word_conditions . ")";

				$order = "sort_order ASC, (CASE WHEN category_id = '{$category_id}' THEN 0 ELSE 99 END), number ASC";

				// Don't search by category - See Pete
				/*if($category_id) {
					$categories = array($category_id);
					$cur_cat = Category::find_by_id($category_id);
					$children = $cur_cat->children();

					foreach($children as $child_cat) {
						$categories[] = $child_cat->id;
					}

					$conditions .= " AND category_id IN (" . implode(", ", $categories) . ")";
				}*/

				$parts = Part::all(array("limit" => $limit, "conditions" => $conditions, "order" => $order));

				return $parts;
		}

		// This is not in the right place. Should be in Category.
		public function category_parts($cat_id) {
			$category = Category::find_by_id($cat_id);
			$categories = $category->children();
			$categories[] = $category;
			$in_array = array();

			foreach($categories as $cat) {
				$in_array[] = $cat->id;
			}

			return Part::find_all_by_category_id($in_array);

		}

		public function add_to_cart($quantity = 1) {
			$cart = QuoteRequest::current();
			return $cart->add_part($this->id, $quantity);
		}
		public function update_quantity_in_cart($quantity) {
			$cart = QuoteRequest::current();
			$cart->update_part_quantity($this->id, $quantity);
		}
		public function remove_from_cart() {
			$cart = QuoteRequest::current();
			$cart->remove_part($this->id);
		}

		public function update_link() {

			$this->link = $this->category->slug . "/";

		}
		public function update_keywords() {
			$category = Category::find($this->category_id);
			$categories = $category->parents();
			$categories[] = $category;
			$keywords = "";
			if(count($categories) > 0) {
				foreach($categories as $c) {
					$keywords .= $c->name . ", ";
				}
			}
			$this->keywords = $keywords;
		}
		public function update_special_search() {
			$this->special_search = preg_replace('/[^a-zA-Z0-9]/', '', $this->number) . preg_replace('/[^a-zA-Z0-9]/', '', $this->model) . preg_replace('/[^a-zA-Z0-9]/', '', $this->keywords) . preg_replace('/[^a-zA-Z0-9]/', '', $this->description) . preg_replace('/[^a-zA-Z0-9]/', '', $this->alternate_numbers);
		}
	}

?>

