<?php
	class QuoteRequest extends AppModel {
		// static $table_name = "quote_requests";
		static $has_many = array(
			array("quote_request_parts", "order" => "part_id"),
		);
		
		public function statuses() {
			$values = self::enum_values("status");
			unset($values[0]);
			return $values;
		}
		
		private static $current_quote_request; // Saves the current user's quote request
		
		/**
		 *	$cart = QuoteRequest::current();
		 *	
		 *	@return object QuoteRequest
		 */
		public static function current() {
			if(self::$current_quote_request) {
				$quote_request = self::$current_quote_request;
			} elseif(cookie_isset("VISITOR_ID")) {
				$quote_request = self::find_by_visitor_id(cookie("VISITOR_ID"));
			}
			
			if(!$quote_request) {
				$this_class = __CLASS__;
				$quote_request = new $this_class();
				$quote_request->status = "Cart";
				$quote_request->visitor_id = quid(20);
				$quote_request->archived = 0;
				$quote_request->save();
				cookie("VISITOR_ID", $quote_request->visitor_id, 30);
			}
			// $quote_request->notify(); // notify, via email, that there is a new quote request
			
			self::$current_quote_request = $quote_request;
			return $quote_request;
		}
		
		public function notify() {
			$id = $this->id;
			load_library("email");
			$message = "See Quote Requests: " . BASE_URL . "/admin/quote-requests/ <br><br>Name: {$this->name} <br>Email: {$this->email}<br>Phone: {$this->phone}<br>Number of Parts: " . array_count($this->quote_request_parts) . "<br>Visitor Came From: " . cookie("VISITOR_REFERER") . "";
			$qrps = QuoteRequestPart::find_all_by_quote_request_id($id);
			if(is_array($qrps)) {
				foreach($qrps as $qrp) {
					$part = Part::find($qrp->part_id);
					$message .= "<p style='border:solid 1px black; padding:10px;'>Number: <a href='" . BASE_URL . "/find-your-part/" . $part->link . $part->slug . "/' target='_blank'><strong>{$part->number}</strong></a>";
					$message .= "<br />Description: {$part->description}<br />";
					$questions = json_decode($qrp->question_answers);
					if(is_array($questions)) {
						foreach($questions as $q) {
							$message .= $q->question . ": " . $q->answer . "<br />";
						}
					}
					$message .= "</p>";
				}
			}
			$message .= "<h3>Feedback:</h3><p>{$this->feedback}</p>";
			phpmailer_send(EMAIL, "New Quote Request from {$this->name} - " . settings("site", "name"), $message, "no-reply@nwdieselparts.com");
		}
		/**
		 *	Adds a part to the current quote request. If the part is already there, returns FALSE.
		 */
		function add_part($part_id, $quantity = 1) {
			$qr_part = QuoteRequestPart::find_by_quote_request_id_and_part_id($this->id, $part_id);
			
			if(!$qr_part) {
				$qr_part = new QuoteRequestPart(array(
					"part_id" => $part_id,
					"quote_request_id" => $this->id,
				));
				return $qr_part->save();
			}
			return FALSE;
		}
		
		function remove_part($part_id) {
			$qr_part = QuoteRequestPart::find_by_quote_request_id_and_part_id($this->id, $part_id);
			if($qr_part) return $qr_part->delete();
			return FALSE;
		}
		
		function update_part_quantity($part_id, $quantity) {
			$qr_part = QuoteRequestPart::find_by_quote_request_id_and_part_id($this->id, $part_id);
			if($qr_part) {
				$qr_part->quantity = $quantity;
				return $qr_part->save();
			}
			return FALSE;
		}
	}
?>