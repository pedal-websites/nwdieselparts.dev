<? layout_open("admin"); ?>
	<?=layout_section("main")?>
		<div class="half">
			<h2>Quick Statistics</h2>
			<div class="today">Today</div>
			<? 
				$today_start = strtotime("today");
				$today_end = strtotime("tomorrow");
				$month_start = strtotime("first day this month");
				$month_end = strtotime("first day next month");
			?>
			<div class="quickstat">
				<strong class="urgent"><a href="/admin/quote-requests/#submitted"><?=count(QuoteRequest::all(array("conditions" => "status = 'Submitted' AND (created_at >= {$today_start} OR updated_at <= {$today_start}) AND (created_at <= {$today_end} OR updated_at >= {$today_end})")))?></a></strong><br />
				Unanswered Quote Requests
			</div>
			<div class="quickstat">
				<strong><?=count(QuoteRequest::all(array("conditions" => "(status = 'Quoted' OR status = 'Accepted' OR status = 'Rejected') AND (created_at >= {$today_start} OR updated_at <= {$today_start}) AND (created_at <= {$today_end} OR updated_at >= {$today_end})")))?></strong><br />
				Quote Requests Answered
			</div>
			<div class="quickstat">
				<strong><?=count(QuoteRequest::all(array("conditions" => "status != 'Cart' AND (created_at >= {$today_start} OR updated_at <= {$today_start}) AND (created_at <= {$today_end} OR updated_at >= {$today_end})")))?></strong><br />
				Total Quote Requests
			</div>
			<!--div class="quickstat">
				<strong>1:03</strong><br />
				Average Response Time
			</div>
			<div class="quickstat">
				<strong>$6,000</strong><br />
				Amount Quoted
			</div-->
			<div class="cleared"></div>
			
			<div class="month">This month</div>
			<div class="quickstat">
				<strong class="urgent"><a href="/admin/quote-requests/#submitted"><?=count(QuoteRequest::all(array("conditions" => "status = 'Submitted' AND (created_at >= {$month_start} OR updated_at <= {$month_start}) AND (created_at <= {$month_end} OR updated_at >= {$month_end})")))?></a></strong><br />
				Unanswered Quote Requests
			</div>
			<div class="quickstat">
				<strong><?=count(QuoteRequest::all(array("conditions" => "(status = 'Quoted' OR status = 'Accepted' OR status = 'Rejected') AND (created_at >= {$month_start} OR updated_at <= {$month_start}) AND (created_at <= {$month_end} OR updated_at >= {$month_end})")))?></strong><br />
				Quote Requests Answered
			</div>
			<div class="quickstat">
				<strong><?=count(QuoteRequest::all(array("conditions" => "status != 'Cart' AND (created_at >= {$month_start} OR updated_at <= {$month_start}) AND (created_at <= {$month_end} OR updated_at >= {$month_end})")))?></strong><br />
				Total Quote Requests
			</div>
			<!--div class="quickstat">
				<strong>1:03</strong><br />
				Average Response Time
			</div>
			<div class="quickstat">
				<strong>$6,000</strong><br />
				Amount Quoted
			</div-->
			<div class="cleared"></div>
			<hr />
			<!--p><a href="#">See more stats</a></p-->
			<p><a href="http://www.google.com/analytics/">Access Google Analytics</a></p>
		</div>
		<div class="half">
			<h2>Incoming Quote Requests</h2>
		</div>
	<?=layout_section_close()?>
<? layout_close(); ?>