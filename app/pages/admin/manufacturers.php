<? 
	if(segments_action() == "delete" && security_check()) {
		$manufacturer = Manufacturer::find_by_id(segments_id());
		$manufacturer->delete();
		redirect("/admin/manufacturers/");
	}
?>
<? layout_open("admin") ?>
	<?=layout_section("main")?>
		<? if(segments_action() == "add" || segments_action() == "edit"): ?>
			<? $manufacturer = (segments_id() > 0) ? Manufacturer::find_by_id(segments_id()) : new Manufacturer; ?>
			<?
				load_library("notice");
				if(form_posted("manufacturer")) {
					$manufacturer->name = form_value("name");
					if(form_is_valid()) {
						if($manufacturer->save()) {
							notice_add("success", "Saved Manufacturer");
							redirect("/admin/manufacturers/");
						} else {
							notice_add("error", "Couldn't save Manufacturer");
						}
					} else {
						notice_add("error", "Please enter valid Manufacturer information");
					}
				}
			?>
			<h2><?=ucwords(segments_action())?> Manufacturer</h2>
			<?=notices_show()?>
			<?=form_open("manufacturer")?>
				<?=form_textbox("name", "Name", $manufacturer->name)?>
				<?=form_button("submit", "", "Save")?>
				<a class="cancel-button" href="<?=previous_url()?>">Cancel & Go Back</a>
			<?=form_close()?>
		<? else: ?>
			<h2>Manufacturers <a href="/admin/manufacturers/add/" class="add-button green">+ ADD</a></h2>
			<p>These are all the manufacturers of your products.  Click one to edit, or use the add button above to create a new one.</p>
			<? $manufacturers = Manufacturer::all(); ?>
			<? if(is_array($manufacturers)): ?>
				<table class="large-list">
					<thead>
						<th>Name</th>
						<th>Actions</th>
					</thead>
					<tbody>
						<? foreach($manufacturers as $m): ?>
							<tr>
								<td><a href="/admin/manufacturers/<?=$m->id?>/edit/"><?=$m->name?></a></td>
								<td><a href="/admin/manufacturers/<?=$m->id?>/delete/" class="delete">Delete</a></td>
							</tr>
						<? endforeach; ?>
					</tbody>
				</table>
			<? endif; ?>
		<? endif; ?>
	<? layout_section_close() ?>
<? layout_close() ?>