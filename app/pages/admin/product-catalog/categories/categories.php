<?php
	
	app_include("functions");
	load_library("image");
	if(segments_action() == "save") {
		Category::save_all();
		redirect("/admin/product-catalog/");
	}
	
 	// Variables
	$category = (segments_id() > 0) ? Category::find_by_id(segments_id()) : new Category();
	$page_url_base = "/admin/product-catalog/categories";
	
	// Deletion Handling
	if(segments_action() == "delete" && security_check()) {
		$category = Category::find(segments_id());
		$redirect_id = "/" . $category->parent_id . "/";
		if($category) {
			if($category->delete()) {
				notice_add("success", "The category was successfully deleted from the system.");
			} else {
				notice_add("error", "An error occured and the category may not have been deleted.");
			}
			redirect($page_url_base . $redirect_id);
		}
	}
	
	// Form Handling
	if(form_posted("category")) {
		if($category) {
			$category_db_fields = array_keys($category->attributes());
			$form_data = form_validate_all($category_db_fields, array(
				"id" => false,
				"parent_id" => "nullify_zero",
				"image" => false
			));
			$category->set_attributes($form_data);
			
			$file = form_get_file("image");
			if(strlen($file['name']) > 1) {
				// Image upload - if they select a file
				$img_path = "/uploads/category-pics/";
				safe_unlink(BASE_FOLDER . $category->image_filename);
				safe_unlink(BASE_FOLDER . $category->image_thumbname);
				safe_unlink(BASE_FOLDER . $category->image_mediumname);
				
				if($uploaded_file = form_file_save("image", BASE_FOLDER . $img_path, "%filename%" . quid(5), array('filetypes' => 'jpg|png|gif|jpeg'))) {
					$category->image_filename = $img_path . $uploaded_file;
					$category->image_thumbname = $img_path . "thumb-" . $uploaded_file;
					$category->image_mediumname = $img_path . "medium-" . $uploaded_file;
					image_resize(BASE_FOLDER . $category->image_filename, BASE_FOLDER . $category->image_thumbname, 215, 150, "crop");
					image_resize(BASE_FOLDER . $category->image_filename, BASE_FOLDER . $category->image_mediumname, 600, 600, "scale");
				}
			}
			
			// Save Category
			if(form_is_valid() && $category->validate_parents() && $category->save()) {
				
				// Save questions
				$q_save_success = true;
				$old_questions = $category->questions;
				$titles = form_value("title");
				$types = form_value("type");
				$options = form_value("options");
				
				if(is_array($titles) && is_array($types) && is_array($options)) {
					foreach($titles as $key => $title) {
						if($title != "") {
							$question = new Question();
							$question->title = $title;
							$question->type = $types[$key];
							$question->options = $options[$key];
							$question->category_id = $category->id;
							if(!$question->save()) $q_save_success = false;
							unlink($question);
						}
					}
				}
				
				// Delete Old Questions
				if($q_save_success) {
					foreach($old_questions as $oq) {
						$oq->delete();
					}
				}
				
				notice_add("success", "Successfully saved " . $category->name);
				redirect($page_url_base . "/{$category->id}/edit/");
			} else {
				notice_add("error", "Something went wrong.");
			}
		}
	}
	if(input_get("parent_id") > 0) $category->parent_id = input_get("parent_id");
?>
<? layout_open("admin"); ?>
	<?=layout_section("main")?>
		<? if(segments_action() == "add"): ?>
			<? include("_category_add.php"); ?>
		<? elseif(segments_action() == "edit"): ?>
			<? include("_category_edit.php"); ?>
		<? else: ?>
			<? redirect("/admin/product-catalog/" . segments_id()); ?>
		<? endif;?>
	<?=layout_section_close()?>
<? layout_close(); ?>