<?=notices_show()?>
<?=form_open("category")?>
	<?=form_textbox("name", "Name", $category->name, array())?>
	<label for="<?=form_field_name("parent_id")?>">Parent Category</label>
	<select name="<?=form_field_name("parent_id")?>">
		<option value="">No Parent Category</option>
		<?=select_options(Category::find_all_top_level(), "subcategories", "id", "name", $category->parent_id)?>
	</select>
	<?=form_textarea("description", "Description", $category->description, array())?>
	<?=form_file("image", "Photo")?>
	<? if(segments_action() == "edit"): ?>
		<? if(strlen($category->image_filename) > 1): ?>
			<h3>Current Photo:</h3>
			<img src="<?=BASE_URL?><?=$category->image_thumbname?>" />
		<? else: ?>
			<p><strong>No photo</strong> has been uploaded for this category!</p>
		<? endif; ?>
	<? endif; ?>
	<div class="cleared"></div>
	<h2>Questions</h2><a href="" id="add-question">Add Question</a>
	<div id="questions">
		<? if(count($category->questions) > 0): ?>
			<? foreach($category->questions as $q): ?>
				<? include("_category_question.php"); ?>
			<? endforeach; ?>
		<? else: ?>
			<? include("_category_question.php"); ?>
		<? endif; ?>
	</div>
	<div class="cleared"></div>
	<button type="submit">Submit</button> <a href="<?=previous_url()?>" class="cancel-button">Cancel</a>
<?=form_close()?>
<? if($category->subcategories): ?>
	<? //=print_pre($category->subcategories)?>
<? endif; ?>