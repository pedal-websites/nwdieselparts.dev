<div class="question">
	<div class="half">
		<?=form_textarea("title[]", "Question", $q->title)?>
		<?=form_select("type[]", "Answer Type", $q->type, array("Text", "Options"), array())?>
		<a class="delete" href="">Delete</a>
	</div>
	<div class="half">
		<?=form_textarea("options[]", "Answer Options", $q->options, array("control_class" => "options"), (($q->type == 0) ? FORM_READONLY : FORM_ENABLED))?>
		<p>Separate by new lines. Test</p>
	</div>
	<div style="clear:both;"></div>
</div>
