<?
	// Product Catalog
	$category = Category::find_by_id(segments_id());
	$page_url_base = "/admin/product-catalog";
	
	$search = "";
	if(input_get("search")) $search = input_get("search");
	if($search != "") {
		$search_parts = Part::search($search, NULL, NULL);
		if(count($search_parts) == 1) {
			redirect($page_url_base . "/parts/" . $search_parts[0]->id . "/edit/");
		}
	}
?>
<? layout_open("admin"); ?>
	<?=layout_section("main")?>
		<div id="submenu" class="cleared">
			<? foreach(Category::find_all_top_level() as $top_c): ?>
				<a href="<?=$page_url_base?>/<?=$top_c->id?>/" <?=(($category->id == $top_c->id) ? "class='current'" : "")?>><?=$top_c->name?></a>
			<? endforeach; ?>
			<a href="<?=$page_url_base?>/categories/add" class="green">+ ADD</a>
			<a href="/admin/product-catalog/categories/0/save" class="green">Update All Links</a>
		</div>
		<hr class="cleared clearboth" />
		<div id="admin-search">
			<?=form_open("", "", "get")?>
				<?=form_textbox("search", "", $search, array("placeholder" => "Search Parts"))?>
			<?=form_close()?>
		</div>
		<hr class="cleared clearboth" />
		<? if($search_parts): ?>
			<h2>Search Parts</h2>
			<ul>
				<? foreach($search_parts as $sp): ?>
					<li><a href="<?=$page_url_base?>/parts/<?=$sp->id?>/edit/"><?=$sp->number?></a></li>
				<? endforeach; ?>
			</ul>
		<? elseif(!$category): // search ?>
			<h2>Product Catalog</h2>
			<p>Cick on a top level category along the top to view sub-categories and parts.</p>
		<? else: ?>
			<div class="category-breadcrumbs">
				<? foreach($category->parents() as $parent): ?>
					<a href="<?=$page_url_base?>/<?=$parent->id?>/"><?=$parent->name?></a> &raquo; 
				<? endforeach; ?>
				<strong><?=$category->name?></strong>
			</div>
			<hr class="cleared clearboth" />
			<h2>Category: <?=$category->name?></h2> <a href="<?=$page_url_base?>/categories/<?=$category->id?>/edit/" class="title-link">Edit</a> | <a href="<?=$page_url_base?>/categories/<?=$category->id?>/delete/" class="delete">Delete</a>
			<div class="cleared"></div>
			<div class="half">
				<h3>Subcategories</h3>
				<p><a href="<?=$page_url_base?>/categories/add/?parent_id=<?=$category->id?>" class="add-button">Create new subcategory</a></p>
				<? if($category->subcategories): ?>
					<table class="large-list">
						<thead>
							<tr>
								<th>Subcategory</th>
								<th>Parts</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							<? foreach($category->subcategories as $sub): ?>
								<tr class="subcategory">
									<td><a href="<?=$page_url_base?>/<?=$sub->id?>/"><?=$sub->name?></a></td>
									<td><?=$sub->parts_count()?></td>
									<td><a href="<?=$page_url_base?>/categories/<?=$sub->id?>/edit/">Edit</a> | <a href="<?=$page_url_base?>/categories/<?=$sub->id?>/delete/" class="delete">Delete</a></td>
								</tr>
							<? endforeach; ?>
						</tbody>
					</table>
				<? endif; ?>
			</div>
			<div class="half">
				<h3>Parts</h3>
				<p><a href="<?=$page_url_base?>/parts/add/?category_id=<?=$category->id?>" class="add-button">Create new part</a></p>
				<? if($category->parts): ?>
					<table class="large-list sortable">
						<thead>
							<tr>
								<th></th>
								<th></th>
								<th>Part #</th>
								<th>Model</th>
								<th>Manufacturer</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							<? foreach($category->parts as $part): ?>
								<tr class="parts" id="parts-<?=$part->id?>">
									<td class="handle">&nbsp;</td>
									<td><img src="<?=$part->image_thumbname?>" height="32px"/></td>
									<td><a href="<?=$page_url_base?>/parts/<?=$part->id?>/edit/"><?=$part->number?></a></td>
									<td><?=$part->model?></td>
									<td><?=$part->manufacturer->name?></td>
									<td><a href="<?=$page_url_base?>/parts/<?=$part->id?>/edit/">Edit</a> | <a href="<?=$page_url_base?>/parts/<?=$part->id?>/delete/" class="delete">Delete</a></td>
								</tr>
							<? endforeach; ?>
						</tbody>
					</table>
				<? endif; ?>
			</div>
		<? endif; ?>
	<?=layout_section_close()?>
<? layout_close(); ?>