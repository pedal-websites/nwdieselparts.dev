<?php

	app_include("functions");
	load_library("image");
	load_library("seo");
	
 	// Variables
	$part = (segments_id() > 0) ? Part::find_by_id(segments_id()) : new Part();
	if(input_get("parent_id") > 0) $part->parent_id = input_get("parent_id");
	$page_url_base = "/admin/product-catalog/parts";
	
	// URL Handling
	if(segments_action() == "list") redirect("/admin/product-catalog/");
	
	// Deletion Handling
	if(segments_action() == "delete" && security_check()) {
		$part = Part::find(segments_id());
		if($part) {
			if($part->delete()) {
				notice_add("success", "The part was successfully deleted from the system.");
			} else {
				notice_add("error", "An error occured and the part may not have been deleted.");
			}
			redirect($page_url_base);
		}
	}
	
	// Form Handling
	if(form_posted("part")) {
		if($part) {
			$part_db_fields = array_keys($part->attributes());
			$form_data = form_validate_all($part_db_fields, array(
				"id" => false,
				"image" => false,
				"category_id" => "nullify_zero",
				"manufacturer_id" => "nullify_zero"
			));
			$part->description = nl2br(form_value("description"));
			$part->set_attributes($form_data);
			$part->slug = slugify($part->number);
			$part->featured = form_validate_checkbox("featured", 1, 0);
			
			$file = form_get_file("image");
			if(strlen($file['name']) > 1) {
				// Image upload - if they select a file
				$explode = explode(".", $file['name']);
				$extension = $explode[1];
				$actual_file_name = slugify($explode[0]);
				$img_path = "/uploads/part-pics/";
				safe_unlink(BASE_FOLDER . $part->image_filename);
				safe_unlink(BASE_FOLDER . $part->image_thumbname);
				safe_unlink(BASE_FOLDER . $part->image_mediumname);
				
				if($uploaded_file = form_file_save("image", BASE_FOLDER . $img_path, $actual_file_name, array('filetypes' => 'jpg|png|gif|jpeg'))) {
					$part->image_filename = $img_path . $uploaded_file;
					$part->image_thumbname = $img_path . "thumb-" . $uploaded_file;
					$part->image_mediumname = $img_path . "medium-" . $uploaded_file;
					image_resize(BASE_FOLDER . $part->image_filename, BASE_FOLDER . $part->image_thumbname, 215, 150, "crop");
					image_resize(BASE_FOLDER . $part->image_filename, BASE_FOLDER . $part->image_mediumname, 600, 600, "scale");
				}
			}
			
			// Save Part
			if(form_is_valid() && $part->save()) {
				notice_add("success", "Successfully saved " . $part->number);
				redirect($page_url_base . "/" . $part->id . "/edit/");
			}
		}
	}
?>
<? layout_open("admin"); ?>
	<?=layout_section("main")?>
		<? if(segments_action() == "add"): ?>
			<? include("_part_add.php"); ?>
		<? elseif(segments_action() == "edit"): ?>
			<? include("_part_edit.php"); ?>
		<? endif;?>
	<?=layout_section_close()?>
<? layout_close(); ?>