<h2>Edit Part <a href="<?=previous_url()?>" class="cancel-button">Cancel &amp; Go Back</a> <a href="/admin/product-catalog/parts/add/?category_id=<?=$part->category_id?>" class="add-button">Add another part to <?=$part->category->name?></a></h2>
<p><a href="/admin/product-catalog/<?=$part->category->id?>/">Go back to <strong><?=$part->category->name?></strong></a></p>
<?=notices_show()?>
<? include("_part_form.php"); ?>