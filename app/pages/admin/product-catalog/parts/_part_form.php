<?
	if(segments_action() == "add") {
		$category_id = input_get("category_id");
	} elseif(segments_action() == "edit") {
		$category_id = $part->category_id;
	}
?>
<div class="cleared"></div>
<div class="two-thirds">
	<?=notices_show()?>
	<?=form_open("part")?>
		<div class="half">
			<?=form_textbox("number", "Number", $part->number, array())?>
			<?=form_textbox("alternate_numbers", "Alternate Numbers/Casting Numbers", $part->alternate_numbers, array())?>
			<label for="<?=form_field_name("category_id")?>">Category</label>
			<select name="<?=form_field_name("category_id")?>">
				<?=select_options(Category::find_all_top_level(), "subcategories", "id", "name", $category_id)?>
			</select>
			<?=form_select("manufacturer_id", "Manufacturer", $part->manufacturer_id, Manufacturer::all(array("order" => "name")), array("option_label" => "name", "option_id" => "id", "option_default" => ""))?>
			<?=form_file("image", "Photo")?>
		</div>
		<div class="half">
			<?=form_textarea("model", "Model", $part->model)?>
			<?=form_textarea("description", "Description", $part->description, array())?>
			<?=form_checkbox("featured", "Featured", $part->featured, array("class" => "featured"))?>
		</div>
		<div class="cleared"></div>
		<button type="submit">Submit</button> <a href="<?=previous_url()?>" class="cancel-button">Cancel</a>
	<?=form_close()?>
</div>
<div class="third">
	<? if(segments_action() == "edit"): ?>
		<? if(strlen($part->image_filename) > 1): ?>
			<h3>Current Photo:</h3>
			<img src="<?=BASE_URL?><?=$part->image_mediumname?>" width="20%" />
		<? else: ?>
			<p><strong>No photo</strong> has been uploaded for this part!</p>
		<? endif; ?>
	<? endif; ?>
</div>