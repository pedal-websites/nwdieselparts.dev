<?
	// meta title |.....................................................................| 70 characters max
	meta("title", "Reset Password");
	// meta description |.....................................................................................................................................................| 150 characters max
	meta("description", "");
	
	app_include("authentication");
?>
<? layout_open("admin"); ?>
	<? layout_section("main"); ?>
		<h1 class="centered">Reset Password</h1>
		<?=notices_show()?>
		<? if(input_get("reset_key") === NULL): ?>
			<?=form_open("forgot_password")?>
				<div class="box">
					<?=form_textbox("email", "Email: ", form_value("email"))?>
					<?=form_button("submit", "", "Send Reset Link")?>
				</div>
			<?=form_close()?>
		<? else: ?>
			<?=form_open("reset_password")?>
				<div class="box">
					<?=form_textbox("reset_key", "Reset Password Key: ", input_get("reset_key"))?>
					<?=form_password("password", "Password: ", "")?>
					<?=form_password("conf_pass", "Confirm: ", "")?>
					<?=form_button("submit", "", "Reset Password")?>
				</div>
			<?=form_close()?>
		<? endif; ?>
	<? layout_section_close(); ?>
<? layout_close(); ?>
