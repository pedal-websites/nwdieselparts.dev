<?php
	// meta title |.....................................................................| 70 characters max
	meta("title", "Edit User");
	// meta description |.....................................................................................................................................................| 150 characters max
	meta("description", "");
	
	if(!user_logged_in()) {
		redirect("/login/");
	}
	
	app_include("authentication");
	
	if(user_is_admin() && segments_id() !== NULL) {
		$current_user = user_get_by_id(segments_id());
	} elseif(segments_action() == "add") {
		// do nothing
	} else {
		$current_user = user_get_by_id(user_id());
	}
	
	if(segments_action() == "delete") {
		user_delete_by_id(segments_id());
		if($current_user['id'] == user_id()) {
			redirect(segments_full() . "/logout=true");
		} else {
			redirect("/admin/users/");
		}
	}
	
	if(form_posted("edit_user")) {
		$update_array = array(
			"username" => form_validate("username"),
			"display_name" => form_validate("display_name"),
			"email" => form_validate("email"),
			"is_admin" => form_validate_checkbox("is_admin"),
			"banned" => form_validate_checkbox("banned")
		);
		$password = form_validate("password");
		$conf_pass = form_validate("conf_pass");
		if($password) {
			if($password == $conf_pass) {
				if(segments_action() == "add") {
					$user_id = user_create($update_array['username'], $update_array['email'], $update_array['display_name'], $update_array['password']);
					$current_user = user_get_by_id($user_id);
				} elseif(user_set_password($current_user['id'], $password)) {
					notice_add("success", "Password was successfully changed.");
				} else {
					notice_add("error", "Error updating Password.");
				}
			} else {
				notice_add("error", "Passwords do not match, please try again.");
			}
		} else {
			notice_add("error", "Password was not updated");
		}
		if(user_update_by_id($current_user['id'], $update_array)) {
			notice_add("success", "Your changes were successfully saved.");
			
			// Refresh with latest info
			$current_user = user_get_by_id($current_user['id'], TRUE);
		} else {
			notice_add("error", "There was an error saving your changes.");
		}
	}
?>
<? layout_open("admin"); ?>
	<? layout_section("main"); ?>
		<? if(segments_action() == "list" || segments_action() == ""): ?>
			<? $users = user_rows();?>
			<h2>Active Users <a href="/admin/users/add/" class="add-button green">+ ADD</a></h2>

			<? if(is_array($users)): ?>
				<table class="large-list">
					<thead>
						<th>ID</th>
						<th>Display Name</th>
						<th>Admin</th>
						<th>Email</th>
						<th>Phone</th>
					</thead>
					<tbody>
						<? foreach($users as $user): ?>
							<tr>
								<td><?=$user['id']?></td>
								<td><a href="/admin/users/<?=$user['id']?>/edit/"><?=$user['display_name']?></a></td>
								<td><?=($user['is_admin']) ? "Yes" : "No"; ?></td>
								<td><?=$user['email']?></td>
								<td><?=$user['phone']?></td>
							</tr>
						<? endforeach; ?>	
					</tbody>
				</table>
			<? endif; ?>
		<? else: ?>
			<h1 class="centered"><?=ucwords(segments_action())?> User</h1>
			<?=notices_show()?>
			<?=form_open("edit_user")?>
				<div class="box">
					<? if(segments_action() == "add"): ?>
						<?=form_textbox("username", "Username: ", $current_user['username'])?>
					<? endif; ?>
					<?=form_textbox("display_name", "Full Name: ", $current_user['display_name'])?>
					<?=form_textbox("email", "Email: ", $current_user['email'])?>			
					<?=form_password("password", "New Password: (Leave blank for no change)")?>
					<?=form_password("conf_pass", "Confirm New Password: ")?>
					<?=form_checkbox("is_admin", "User is Admin?", $current_user['is_admin'])?>
					<?=form_checkbox("banned", "User is Banned?", $current_user['banned'])?>
					<?=form_button("submit", "", "Save Changes", array("control_class" => "large_button button_blue"))?>
					<a href="/admin/users/<?=$current_user['id']?>/delete/" class="" onclick="return confirm('Are you sure you want to delete your user information? There is NO UNDO')">Delete this account</a>
					<div class="clearboth"></div>
				</div>
			<?=form_close()?>
		<? endif; ?>
	<? layout_section_close(); ?>
<? layout_close(); ?>