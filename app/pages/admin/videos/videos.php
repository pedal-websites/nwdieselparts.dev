<?
	load_library("form");
	
	if(segments_action() == "add" || segments_action() == "edit") {
		if(form_posted("video")) {
			if(segments_action() == "add") {
				$video = New Video();
			} else {
				$video = Video::find_by_id(segments_id());
			}
			$video->title = form_validate("title");
			$video->video_hash = form_validate("video_hash");
			$video->thumbnail_id = form_validate("thumbnail_id");
			if(form_is_valid()) {
				if($video->save()) {
					notice_add("success", "Saved changes");
					if(segments_action() == "add") {
						redirect("/admin/videos/{$video->id}/edit/");
					}
				} else {
					notice_add("error", "Couldn't save changes");
				}
			} else {
				notice_add("error", "Please fill out all fields");
			}
		}
	} elseif(segments_action() == "delete") {
		if($video = Video::find_by_id(segments_id())) {
			if($video->delete()) {
				notice_add("success", "Removed video");
				redirect("/admin/videos/");
			} else {
				notice_add("error", "Couldn't remove video. Please try again later");
				redirect("/admin/videos/");
			}
		}
	} else {
		$videos = Video::all();
	}
?>
<? layout_open("admin"); ?>
	<?=layout_section("main")?>
		<? if(segments_action() == "add" || segments_action() == "edit"): ?>
			<h2><?=ucwords(segments_action())?> Video</h2>
			<div class="cleared"></div>
			<? if(segments_id()) $video = Video::find_by_id(segments_id()); ?>
			<div class="two-thirds">
				<?=notices_show()?>
				<?=form_open("video"); ?>
					<?=form_textbox("title", "Title: ", $video->title);?>
					<?=form_textbox("video_hash", "Video Hash: ", $video->video_hash, array("help" => "<strong>Example:</strong> http://www.youtube.com/watch?v=<strong class='red'>6efvqR97Kz4</strong>"));?>
					<?=form_select("thumbnail_id", "Thumbnail: ", ($video->thumbnail_id) ? $video->thumbnail_id : 2, Video::enum_values("thumbnail_id"), array("key_same_as_label" => TRUE));?>
					<?=form_button("submit", "", "Save"); ?>
					<a href="/admin/videos/" class="cancel-button">Cancel</a>
				<?=form_close(); ?>
			</div>
			<div class="one-third">
				<? if($video->video_hash): ?>
					<h3>Thumbnail Options</h3>
					<span class="large-font red">1: </span><img src="http://img.youtube.com/vi/<?=$video->video_hash?>/1.jpg"><br />
					<span class="large-font red">2: </span><img src="http://img.youtube.com/vi/<?=$video->video_hash?>/2.jpg"><br />
					<span class="large-font red">3: </span><img src="http://img.youtube.com/vi/<?=$video->video_hash?>/3.jpg">
				<? else: ?>
					<span>Save video to see thumbnails</span>
				<? endif; ?>
			</div>
		<? else: ?>
			<h2>Manage Videos</h2>
			<?=notices_show()?>
			<p><a href="/admin/videos/add/" class="add-button">Add Video</a></p>
			<? if(is_array($videos)): ?>	
				<table>
					<thead>
						<tr>
							<th>Title</th>
							<th>Video Hash</th>
							<th>Thumbnail</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<? foreach($videos as $v): ?>
							<tr>
								<td><a href="/admin/videos/<?=$v->id?>/edit/"><?=$v->title?></a></td>
								<td><?=$v->video_hash?></td>
								<td><img src="http://img.youtube.com/vi/<?=$v->video_hash?>/<?=$v->thumbnail_id?>.jpg" height="50" /></td>
								<td><a href="/admin/videos/<?=$v->id?>/edit/">Edit</a> | <a href="/admin/videos/<?=$v->id?>/delete/" class="delete" data-confirm="Are you sure you want to remove this video?">Delete</a></td>
							</tr>
						<? endforeach; ?>
					</tbody>
				</table>
			<? endif; ?>
		<? endif;?>
	<?=layout_section_close()?>
<? layout_close(); ?>