<?php
	$submitted_status = input_post("status");
	$status = (($submitted_status) ? ucfirst($submitted_status) : "Submitted");
	$requests = QuoteRequest::find_all_by_status($status);
	load_library("string_format");
?>

<h2><?=$status?> Quote Requests (<?=count($requests)?>)</h2>
<table id="quote-requests">
	<thead>
		<th>Name</th>
		<th>Email</th>
		<th>Phone</th>
		<th>Feedback</th>
		<th># of Parts</th>
		<th>Submitted</th>
		<th>Print</th>
	</thead>
	<tbody>
		<? foreach($requests as $request): ?>
			<?php
				// Generate JSON for jQuery
				$request_array = $request->attributes();
				unset($request_array["created_at"]);
				unset($request_array["updated_at"]);
				$parts_array = array();
				foreach($request->quote_request_parts as $key => $qpr) {
					$part = Part::find_by_id($qpr->part_id);
					$parts_array[$key] = $qpr->attributes();
					$parts_array[$key]['number'] = $part->number;
					$parts_array[$key]['question_answers'] = json_decode($qpr->question_answers, true);
					$parts_array[$key]['description'] = htmlspecialchars($part->description);
					unset($parts_array[$key]["created_at"]);
					unset($parts_array[$key]["updated_at"]);
				}
				$request_array["parts"] = $parts_array;
			?>
			<tr class="request" data-request='<?=json_encode($request_array)?>' href="#?w=800" rel="request_modal" title="Click to view">
				<td><?=$request->name?></td>
				<td><?=$request->email?></td>
				<td><?=$request->phone?></td>
				<td><?=$request->feedback?></td>
				<td><?=count($request->quote_request_parts)?> Parts</td>
				<td><?=($request->submited_date) ? string_friendly_date(strtotime($request->submited_date)) : string_friendly_date(strtotime($request->created_at));?></td>
				<td><a href="/admin/quote-requests/print/<?=$request->id?>/" target="_blank">Print</a></td>
			</tr>
		<? endforeach; ?>
	</tbody>
</table>
