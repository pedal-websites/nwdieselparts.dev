<?
	$request = QuoteRequest::find_by_id(segments_id());
?>
<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><?=meta('title')?></title>
		<?=canonical()?>

		<? register_css("print", "style/print.css", "all", 10); ?>
		<? head_hook(); ?>
	</head>
	<body>
		<div class="container">
			<? if($request): ?>
				<p class="fr">Printed: <?=date("F j, Y")?></p>
				<div class="clearboth"></div>
				<h2>Quote Request</h2>
				<hr />
				<table class="info">
					<thead>
						<th>Name</th>
						<th>Phone #</th>
						<th>Email</th>
						<th>Status</th>
						<th>Date Requested</th>
					</thead>
					<tbody>
						<td><?=$request->name?></td>
						<td><?=$request->phone?></td>
						<td><?=$request->email?></td>
						<td><?=$request->status?></td>
						<td><?=$request->created_at->format("F j, Y")?></td>
					</tbody>
				</table>
				<? foreach($request->quote_request_parts as $p): ?>
					<hr />
					<p>Part info: <strong><?=$p->part->number?></strong> - <?=$p->part->description?></p>
					<? $questions = json_decode($p->question_answers); ?>
					<? if($questions): ?>
						<p class="questions">
							<? foreach($questions as $q): ?>
								<?=$q->question?>: <strong><?=$q->answer?></strong><br />
							<? endforeach; ?>
						</p>
					<? endif; ?>
				<? endforeach; ?>
			<? else: ?>
				<?=notice_show("error", "Error - quote request not found")?>
			<? endif; ?>
		</div>
		<? foot_hook(); ?>
	</body>
</html>