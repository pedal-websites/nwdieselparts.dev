<?
	load_library("form");
	if(form_posted("email_customer", false)) {
		$to = form_validate("email_to", "trim|val_email");
		$subject = form_validate("subject", "trim|val_required");
		$cc = form_validate("cc", "trim");
		$message_content = form_validate("message_content", "trim|val_required");
		
		if(form_is_valid()) {
			load_library("email");	
			$message = nl2br($message_content);
			$options['cc'] = $cc;
			$from = EMAIL;
			if(phpmailer_send($to, $subject, $message, $from, $options)) {
				notice_add("success", "Successfully sent email to " . $to);
				redirect("/admin/quote-requests/sent/");
			} else {
				notice_add("error", "Didn't send email to " . $to . ". Check and make sure you put a subject and message.");
			}
		} else {
			notice_add("error", "Didn't send email to " . $to . ". Check and make sure you put a subject and message.");
		}
	}

?>
<? layout_open("admin"); ?>
	<?=layout_section("main")?>
		<?=notices_show()?>
		<div id="submenu">
			<? foreach(QuoteRequest::statuses() as $status): ?>
				<a href="#<?=strtolower($status)?>" class="<?=((TRUE)? "" : "")?>"><?=$status?></a>
			<? endforeach; ?>
		</div>
		<div class="cleared"></div>
		<hr />
		<div id="request_modal" class="popup_block" >
			<div class="popup_content">
				<h2>Manage Quote Request</h2>
				<table class="info">
					<thead>
						<th>Name</th>
						<th>Phone #</th>
						<th>Email</th>
					</thead>
					<tbody>
						<td class="name"></td>
						<td class="phone"></td>
						<td class="email"></td>
					</tbody>
				</table>
				<div style="clear:both;"></div>
				<div class="half">
					<h3>Write an Email</h3>
					<?=form_open("email_customer")?>
						<?=form_textbox("email_to", "To:")?>
						<?=form_textbox("subject", "Subject: ", form_value("subject"))?>
						<?=form_textbox("cc", "CC: ", form_value("cc"))?>
						<?=form_textarea("message_content", "Message: ", form_value("message_content"))?>
						<button type="submit">Email!</button>
					<?=form_close()?>
				</div>
				<div class="half">
					<h3>Change Status</h3>
					<div class="statuses">
						<? foreach(QuoteRequest::statuses() as $status): ?>
							<button type="button" value="<?=$status?>"><?=$status?></button>
						<? endforeach; ?>
						<p>Current Status: <span class="current-status"><strong>Submitted</strong></span></p>
					</div>
					<h3>Parts</h3>
					<ul class="parts">
						<!-- Loaded via jQuery -->
					</ul>
					<h3>Visitor Came From</h3>
					<a class="referrer" target="_blank" href="javascript:;">Referring URL</a>
					<h3>Feedback</h3>
					<p class="feedback"></p>
				</div>
				<div class="cleared"></div>
			</div>
		</div>
		<div id="quote-request-feed">
			<!-- Filled by jQuery and ajax -->
			Loading...
		</div>
	<?=layout_section_close()?>
<? layout_close(); ?>