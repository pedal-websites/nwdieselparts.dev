		<? // figure out correct modifier to use
			$char = strtolower(substr($part->number, 0, 1));
			if ($char == "a" || $char == "e" || $char == "i" || $char == "o" || $char == "u") {
				$modifier = "an";
			} else {
				$modifier = "a";
			}
		?>

		<? meta("description", "Northwest Diesel Parts offers a wide variety of diesel parts. Get a free quote on {$modifier} {$part->number} in seconds!"); ?>
		<? /*<div class="breadcrumbs"><?=segments_breadcrumbs("&gt;")?></div> */ ?>
		<h3 class="h3-mobile-styles"><?=$part->number?><a href="<?=previous_url()?>" class="back-link">Back</a></h3>
		<?=notices_show()?>
		<div class="cleared"></div>
		<div class="part-img">
			<? if(strlen($part->image_mediumname) > 1): ?>
				<img src="<?=$part->image_mediumname?>" alt='<?=$part->number?> - <?=$part->description?>' />
			<? elseif(strlen($category->image_mediumname) > 1):?>
				<img src="<?=$category->image_mediumname?>" alt='<?=$part->number?> - <?=$part->description?>' />
			<? endif; ?>
			<div class="part-info-below">
				<table>
					<tr>
						<td>Part Description:</td>
						<td><?=nl2br($part->description)?></td>
					</tr>
					<tr>
						<td>Alternate/Casting Numbers:</td>
						<td><strong><?=($part->alternate_numbers) ? $part->alternate_numbers : "No alternate/casting numbers"; ?></strong></td>
					</tr>
					<tr>
						<td>Model:</td>
						<td><strong><?=($part->model) ? nl2br($part->model) : "No model number"; ?></strong></td>
					</tr>
				</table>
			</div>
		</div>
		<div class="part-info-right">
			<a href="#?w=500" rel="part_questions" class="green-button add-to-cart">Get Price Quote</a>
			<p class="small-text message info"><em>Click the "Get Price Quote" button above to give us information about the equipment you need this part for.  You can then get a price quote.  You can request a quote for multiple parts from us at the same time, so add as many parts to your quote list as you need!</em></p>
			<p class="small-text message info"><strong>Why We Don't Have Pricing Online</strong></p>
			<p class="small-text message info">
				There are many variables that determine the correct parts for your engine. Our sales staff have 25+ years experience on average in the diesel engine parts field. Let our experts determine the correct parts for your needs and find the lowest cost and highest quality parts to get your engine up and running as soon as possible, with no hidden costs!
			</p>
		</div>

		<div id="part_questions" class="popup_block">
			<div class="popup_content">
				<h2>Additional Information</h2>
				<div class="nice-info">
					<p>In order to provide you with an accurate and timely quote, please fill out the below questions.  Most fields are required. Thanks!</p>
				</div>
				<?php
					$current_category = Category::find($part->category_id);
					$categories = $current_category->parents();
					$categories = array_reverse($categories);
					$categories[] = $current_category; // add current category to array
					$no_questions = true;
				?>
				<?=form_open("add_to_cart", "/cart/")?>
					<?=form_hidden("part_id", $part->id)?>
					<? foreach($categories as $cat): ?>
						<? $questions = Question::find_all_by_category_id($cat->id); ?>
						<? if($questions) $no_questions = false; ?>
						<? if($questions): ?>
							<? foreach($questions as $q): ?>
								<? if($q->type == 0): ?>
									<?=form_textbox("answers[{$q->id}]", ucwords($q->title))?>
								<? elseif($q->type == 1): ?>
									<?=form_select("answers[{$q->id}]", ucwords($q->title), NULL, explode("\n", $q->options), array("key_same_as_label" => true))?>
								<? endif; ?>
							<? endforeach; ?>
						<? endif; ?>
					<? endforeach; ?>
					<div class="cleared"></div>
					<? if($no_questions): // Javascript will submit form without popup ?>
						<span class="no-questions"></span>
					<? endif; ?>
					<?=form_button("submit", "", "Save Information & Continue", array("control_class" => "button green-button",  "class" => "fr")); ?>
				<?=form_close()?>
			</div>
		</div>