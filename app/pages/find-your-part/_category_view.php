			<? meta("title", "{$category->name} | Northwest Diesel Parts"); ?>
			<? meta("description", $category->description); ?>

			<h3><?=$category->name?></h3>
			<? if($category->description): ?>
				<div class="info-notice">
					<p>Read a description of what we offer for this particular type of part below:</p>
				</div>
				<p><?=nl2br($category->description)?></p>
			<? endif; ?>
			<? if($category->children() && !$search): ?>
				<div class="cleared">
					<h3>Sub-Categories</h3>
					<div class="info-notice">
						<p>We offer the following types of <?=$category->name?>.  Click on one of the links below to view parts and subcategories of that type. </p>
					</div>
					<?php
						$children = $category->children(); // Find the children
					?>
					<? if($children): // Skip to else if cat has no children?>
						<?php 
							$parents = $category->parents(); // Find the category parents
							$siblings = Category::find_all_by_parent_id($category->parent_id); // Find the sibling categories at the same level
							$parent = Category::find($category->parent_id);
							// Build parent category url
							
						?>
						<? if(count($children) > 0): // Show category and children ?>
								<ul class="categories">
									<? foreach($children as $child): ?>
										<li><a href="/find-your-part/<?=$child->slug?>/" ><?=$child->name?></a></li>
									<? endforeach; ?>
								</ul>
						<? endif; ?>
					<? endif; ?>
				</div>
			<? endif; ?>
			<? if($category->parts || $search): ?>
				<div id="parts-search" class="category-parts-search">	
					<h3>Parts</h3>
					<?=form_open("", "#parts-search", "get", array("class" => "category_search"))?>
						<?=form_textbox("search", "", $search, array("control_class" => "search-input", "placeholder" => "Search this category, then press enter"))?>
						<button type="submit" class="blue-button">Search</button>
					<?=form_close()?>
				</div>
				<div class="cleared"></div>
				<div class="info-notice">
					<p>Here are the parts we offer in this category; click on the title of a part to see a picture and add it to your quote!</p>
				</div>
				<div id="search-parts-area" class="cleared">
					<? /*
						<?=form_open("", "", "get")?>
							<?=form_textbox("search", "", $search, array("control_class" => "search-input", "placeholder" => "Search this category, then press enter"))?>
							<button type="submit" class="blue-button">Search</button>
						<?=form_close()?>
					*/ ?>
					<div class="results-2-col cleared">		
						<?=notices_show();?>
						<?=parts_list($category->id, $search)?>
					</div>
				</div>
			<? endif; ?>
