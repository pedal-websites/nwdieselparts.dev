<?
	load_library("form");

	meta("title", "Find a Diesel Engine Part | Northwest Diesel Parts");
	// meta description |.....................................................................................................................................................| 150 characters max
	meta("description", "Use our quick and easy search and drill-down categories to find a part for your diesel equipment.  Get a free quote!");

	if(form_posted("search_parts")) {
		$search = form_validate("search");
		if(!form_is_valid()) die("Hacking attempt detected");
	}



	if(input_get("search")) {$search = input_get("search");}
	if($search) {notice_add("success", "Thanks for your search!  Here are your results for '{$search}':");}

	$parent_categories = Category::find_all_top_level();

	if(segments_last() != "") {
		$part = Part::find_by_slug(segments_last());
		$category = Category::find_by_slug(rtrim(str_replace("find-your-part/", "", segments_full()), "/"));
	}

	if($part) {
		meta("title", $part->number . " | " . settings("site", "name"));
	}
	app_include("functions");
?>
<? layout_open("default"); ?>
	<? layout_section("page-title"); ?>
		<h1>Product Line</h1>
		<p>We have a large inventory of diesel engine parts in stock. If we don’t stock the parts you need, we can source the parts that you're after from one of our many suppliers. We'd love to help you find your parts! Give us a try and find out why our customers are customers for life. You can’t go wrong with Northwest Diesel Parts.</p>
	<? layout_section_close(); ?>
	<? layout_section("full-width"); ?>

		<? if($part OR $category OR $search): ?><div class="breadcrumbs"><?=segments_breadcrumbs("&gt;")?></div><? endif;?>
		<? if($part): ?>
			<? include("_part_view.php"); ?>
		<? elseif($category): ?>
			<? include("_category_view.php"); ?>
		<? else: ?>
			<? include("_default_view.php"); ?>
		<? endif; ?>
		<? if(!$part): ?>
		<? endif; ?>

	<? layout_section_close(); ?>
<? layout_close(); ?>
