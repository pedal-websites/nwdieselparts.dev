<a href="/find-your-part/" class="fr">start over</a>
<h2>Categories</h2> 
<ul class="sidebar-menu submenu">
	<?php
		$current_cat = (($part) ? Category::find($part->category_id) : $category); // Determine current category if part or category page
		if($current_cat) $children = Category::find_all_by_parent_id($current_cat->id); // Find the children
	?>
	<? if($current_cat && !($current_cat->parent_id == 0 && !$children)): // Skip to else if parent cat that has no children or no cat defined ?>
		<?php 
			$parents = $current_cat->parents(); // Find the category parents
			$siblings = Category::find_all_by_parent_id($current_cat->parent_id); // Find the sibling categories at the same level
			$parent = Category::find($current_cat->parent_id);
			// Build parent category url
			
		?>
		<? if(count($children) > 0): // Show category and children ?>
			<li>
				<a href="/find-your-part/<? /*<?=$parent_slugs?><?=$current_cat->slug?>/*/ ?>" class="current">&gt; <?=$current_cat->name?></a>
				<ul class="sidebar-menu submenu">
					<? foreach($children as $child): ?>
						<li><a href="/find-your-part/<? /*<?=$parent_slugs?><?=$category->slug?>/ */?><?=$child->slug?>/" >&gt; <?=$child->name?></a></li>
					<? endforeach; ?>
				</ul>
			</li>
		<? else: // Show parent category and siblings ?>
			<li>
				<a href="/find-your-part/<?=$parent->slug?>/">&gt; <?=$parent->name?></a></li>
				<ul class="sidebar-menu submenu">
					<? foreach($siblings as $sib): ?>
						<li><a href="/find-your-part/<? /*<?=$parent_slugs?> */?><?=$sib->slug?>/" class="<?=(($sib->id == $current_cat->id) ? "current" : "")?>">&gt; <?=$sib->name?></a></li>
					<? endforeach; ?>
				</li>
			</li>
		<? endif; ?>
	<? else: // Show top-level categories ?>
		<? foreach($parent_categories as $c): ?>
			<li><a href="/find-your-part/<?=$c->slug?>/" class="<?=(($c->id == $current_cat->id) ? "current" : "")?>">&gt; <?=$c->name?></a></li>
		<? endforeach; ?>
	<? endif; ?>
	
	
	
	
	
	<? /* if($part): ?>
		<? $part_parent = Category::find($part->category_id); ?>
		<? $parents = $part_parent->parents(); ?>
		<? foreach(array_reverse($parents) as $p): ?>
			<? if($p->name) $parent_name = $p->name; ?>
			<? $parent_slugs .= $p->slug . "/"; ?>
		<? endforeach; ?>
		<? $siblings = Category::find_all_by_parent_id($part_parent->parent_id); ?>
		<? if($parent_name): ?>
			<li>
				<a href="/find-your-part/<?=$parent_slugs?>">&gt; <?=$parent_name?></a></li>
				<ul class="sidebar-menu submenu">
					<? foreach($siblings as $cat): ?>
						<li><a href="/find-your-part/<?=$parent_slugs?><?=$cat->slug?>/" class="<?=($cat->slug == $part_parent->slug) ? "current" : "1" ;?>">&gt; <?=$cat->name?></a></li>
					<? endforeach; ?>
				</li>
			</li>
		<? else: ?>
			<? foreach($siblings as $cat): ?>
				<li><a href="/find-your-part/<?=$parent_slugs?><?=$cat->slug?>/">&gt; <?=$cat->name?></a></li>
			<? endforeach; ?>
		<? endif; ?>
	<? elseif($category): ?>
		<? $children = Category::find_all_by_parent_id($category->id); ?>
		<? $parents = $category->parents(); ?>
		<? foreach(array_reverse($parents) as $p): ?>
			<? if($p->name) $parent_name = $p->name; ?>
			<? $parent_slugs .= $p->slug . "/"; ?>
		<? endforeach; ?>
		<li>
			<a href="/find-your-part/<?=$parent_slugs?><?=$category->slug?>">&gt; <?=$category->name?></a>
			<ul class="sidebar-menu submenu">
				<? foreach($children as $child): ?>
					<li><a href="/find-your-part/<?=$parent_slugs?><?=$category->slug?>/<?=$child->slug?>/">&gt; <?=$child->name?></a></li>
				<? endforeach; ?>
			</ul>
		</li>
	<? else: ?>
		<? foreach($parent_categories as $c): ?>
			<li><a href="/find-your-part/<?=$c->slug?>/">&gt; <?=$c->name?></a></li>
		<? endforeach; ?>
	<? endif;  */?>
		
		
		
		
		
	<? /* if(2==1): // Current Category ONLY?>
		<? $children = Category::find_all_by_parent_id($category->id); ?>
		<h2>Categories</h2>
		<ul class="sidebar-menu submenu">
			<? if(segments_page() != segments_full() && !$part): ?>
				<? if(is_array($children)): ?>
					<li class="has-children">
				<? else: ?>
					<li>
				<? endif; ?>
					<a href="./">&gt; <?=$category->name?></a>
					<? if(is_array($children)): ?>
						<ul class="sidebar-menu submenu">
							<? foreach($children as $child): ?>
								<li><a href="./<?=$child->slug?>/">&gt; <?=$child->name?></a></li>
							<? endforeach; ?>
						</ul>
					<? endif; ?>
				</li>
			<? elseif($part): ?>
				<? $category = Category::find($part->category_id); ?>
				<? $parent = Category::find($category->parent_id); ?>
				<? if($parent): ?>
					<li>
						<a href="../../">&gt; <?=$parent->name?></a>
						<ul class="sidebar-menu submenu">
							<li><a href="../">&gt; <?=$category->name?></a></li>
						</ul>
					</li>
				<? else: ?>
					<li>
						<a href="../">&gt; <?=$category->name?></a></li>
					</li>
				<? endif; ?>
			<? else: ?>
				<? foreach($parent_categories as $c): ?>
					<li><a href="/find-your-part/<?=$c->slug?>/">&gt; <?=$c->name?></a></li>
				<? endforeach; ?>
			<? endif; ?>
		</ul>
	<? else: // Expandable List ?>
		<h2>Categories</h2>
		<ul class="sidebar-menu submenu">
			<? if(is_array($parent_categories)): ?>
				<? foreach($parent_categories as $c): ?>
					<li>
						<a href="/find-your-part/<?=$c->slug?>/">&gt; <?=$c->name?></a>
						<? if(in_array($c->slug, explode("/", segments_full()))): ?>
							<ul class="sidebar-menu submenu">
								<? $sub_categories = Category::find_all_by_parent_id($c->id); ?>
								<? foreach($sub_categories as $sc): ?>
									<li>
										<a href="/find-your-part/<?=$c->slug?>/<?=$sc->slug?>/">&gt; <?=$sc->name?></a>
										<? if(in_array($sc->slug, explode("/", segments_full()))): ?>
											<ul class="sidebar-menu submenu">
												<? $sub_sub_categories = Category::find_all_by_parent_id($sc->id); ?>
												<? foreach($sub_sub_categories as $ssc): ?>
													<li><a href="/find-your-part/<?=$c->slug?>/<?=$sc->slug?>/<?=$ssc->slug?>">&gt; <?=$ssc->name?></a></li>
												<? endforeach; ?>
											</ul>
										<? endif; ?>
									</li>
								<? endforeach; ?>
							</ul>
						<? endif; ?>
					</li>
				<? endforeach; ?>
			<? endif; ?>
		</ul>
	<? endif; */?>
</ul>