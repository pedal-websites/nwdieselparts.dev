<?
/*
    <?=form_open("", "", "get")?>
        <?=form_textbox("search", "", $search, array("control_class" => "search-input", "placeholder" => "Search, then press enter"))?>
        <button type="submit" class="blue-button">Search</button>
    <?=form_close()?>
*/
$imgs = array(
    "Cam Followers" => "/app/media/images/94760.jpg",
    "Camshafts" => "/uploads/part-pics/cams0adbe.jpg",
    "Connecting Rods" => "/uploads/part-pics/crosshd531fd.jpg",
    "Crankshafts (New)" => "/uploads/part-pics/cranks124f9b.jpg",
    "Crankshafts (Reground)" => "/uploads/part-pics/cranks197f63.jpg",
    "Cylinder Blocks" => "/uploads/part-pics/3408 0015302a.jpg",
    "Cylinder Heads" => "/uploads/part-pics/3412 001e9680.jpg",
    "Cylinder Heads-Natural Gas" => "/uploads/part-pics/5149878a36b3.JPG",
    "Engine Oil Coolers" => "/app/media/images/oil-cooler.jpg",
    "Featured Items" => "/app/media/images/remanufactured-long-block-engine-thumb.jpg",
    "Gasket sets" => "/uploads/part-pics/1kit9e491.jpg",
    "In-Frame/Out-of-Frame Engine Kits" => "/uploads/part-pics/3408%200015302a.jpg",
    "Injectors/Nozzles" => "/app/media/images/injectors.gif",
    "Long Blocks & Engines" => "/uploads/part-pics/3208lb-004.jpg",
    "Oil Pumps" => "/app/media/images/Elring-oil-pumps.jpg",
    "Short Blocks" => "/app/media/images/JDM_3V_302E_Shortblock_4.6L_stroker1.jpg",
    "Turbochargers" => "/app/media/images/Turbocharger-and-Supercharger-in-engines.jpg",
    "Water Pumps" => "/app/media/images/car-water-pump-250x250.jpeg"
);
?>
    <h3>Search</h3>
    <div id="search-parts-area" class="cleared">
        <?= form_open("", "", "get", array("class" => "default_search")) ?>
        <?= form_textbox("search", "", $search,
            array("control_class" => "search-input", "placeholder" => "Search this category, then press enter")) ?>
        <button type="submit" class="blue-button">Search</button>
        <?= form_close() ?>
        <?= notices_show(); ?>
        <? if ($category->id || $search): ?>
            <div class="results-2-col cleared">
                <?= parts_list($category->id, $search) ?>
            </div>
        <? endif; ?>
    </div>
<? if (!$search): ?>
    <h3>Choose a Category</h3>
    <?
    $categories = Category::find_all_top_level();
    $split = array_split($categories, 3);
    ?>
    <div class="categories">
        <?php
        for ($i = 0; $i < count($categories); $i++) {
            $item = $categories[$i];
            ?>
            <div class="category-item">
                <a class="category-link" href="/find-your-part/<?= $item->slug ?>/">
                    <span class="category-title"><?= $item->name ?></span>
                    <img class="category-img" src="<?php echo $imgs[$item->name] ?>">
                </a>
            </div>
            <?php
        }
        ?>
    </div>
<? endif; ?>
<? $featured_parts = featured_parts($category); ?>
<? if (!$search && array_count($featured_parts) > 0): ?>
    <h3>Featured Parts</h3>
    <?= featured_parts_html($featured_parts) ?>
    <div class="cleared"></div>
<? endif; ?>

<?
/*
    <div class="results-2-col cleared">
        <?=parts_list(NULL, $search, 100)?>
    </div>
*/
?>