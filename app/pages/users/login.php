<?
	// meta title |.....................................................................| 70 characters max
	meta("title", "User Login");
	// meta description |.....................................................................................................................................................| 150 characters max
	meta("description", "");
	
	app_include("authentication");
?>
<? layout_open("default"); ?>
	<? layout_section("content"); ?>
		<?=notices_show()?>
		<? if(user_logged_in()): ?>
			<p><a href="/users/login/?logout=true">Log Out</a></p>
			<? if(user_is_admin()): ?>
				<? redirect("/admin/"); ?>
			<? endif; ?>
		<? else: ?>
			<?=form_open("login")?>
				<?=form_textbox("username", "Email Address", form_value("email"))?>
				<?=form_password("password", "Password")?>
				<?=form_checkbox("remember", "Remember me", form_value("remember"), array("class" => "inline-field"))?>
				<?=form_button("submit", "", "Log In")?> <a href="/users/reset/" class="remember-me">Forgot Password?</a>
			<?=form_close()?>
		<? endif; ?>
	<? layout_section_close(); ?>
<? layout_close(); ?>
