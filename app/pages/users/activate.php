<?
	// meta title |.....................................................................| 70 characters max
	meta("title", "User Activation");
	// meta description |.....................................................................................................................................................| 150 characters max
	meta("description", "");
	
	if(user_logged_in()) user_logout();
	
	app_include("authentication");
?>
<? layout_open("default"); ?>
	<? layout_section("content"); ?>
		<?=notices_show()?>
		<a href="<?=$_SERVER['REQUEST_URI']?>">Reload this page</a>
	<? layout_section_close(); ?>
<? layout_close(); ?>
