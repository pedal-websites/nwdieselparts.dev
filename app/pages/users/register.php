<?
	// meta title |.....................................................................| 70 characters max
	meta("title", "User Registration");
	// meta description |.....................................................................................................................................................| 150 characters max
	meta("description", "");
	
	app_include("authentication");
?>
<? layout_open("default"); ?>
	<? layout_section("content"); ?>
		<?=notices_show()?>
		<? if(user_logged_in()): ?>
			<p><a href="/users/?logout=true">Log Out</a></p>
		<? else: ?>
			<?=form_open("register")?>
				<?=form_textbox("display_name", "Full Name: ", form_value("display_name"))?>
				<?=form_textbox("email", "Email: ", form_value("email"))?>
				<? if(settings('users', 'login_type') == "username"): ?>
					<?=form_textbox("username", "Username: ", form_value("username"))?>
				<? endif; ?>
				<?=form_password("password", "Password: ", "")?>
				<?=form_password("conf_pass", "Confirm: ", "")?>
				<?=form_button("submit", "", "Register")?>
			<?=form_close()?>
		<? endif; ?>
	<? layout_section_close(); ?>
<? layout_close(); ?>


