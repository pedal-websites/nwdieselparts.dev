<?
	// meta title |.....................................................................| 70 characters max
	meta("title", "Reset Password");
	// meta description |.....................................................................................................................................................| 150 characters max
	meta("description", "");
	
	app_include("authentication");
?>
<? layout_open("default"); ?>
	<? layout_section("content"); ?>
		<?=notices_show()?>
		<? if(input_get("reset_key") === NULL): ?>
			<?=form_open("forgot_password")?>
				<?=form_textbox("email", "Email: ", form_value("email"))?>
				<?=form_button("submit", "", "Send Reset Link")?>
			<?=form_close()?>
		<? else: ?>
			<?=form_open("reset_password")?>
				<?=form_textbox("reset_key", "Reset Password Key: ", input_get("reset_key"))?>
				<?=form_password("password", "Password: ", "")?>
				<?=form_password("conf_pass", "Confirm: ", "")?>
				<?=form_button("submit", "", "Reset Password")?>
			<?=form_close()?>
		<? endif; ?>
	<? layout_section_close(); ?>
<? layout_close(); ?>
