<?php
	// meta title |.....................................................................| 70 characters max
	meta("title", "Edit User");
	// meta description |.....................................................................................................................................................| 150 characters max
	meta("description", "");
	
	if(!user_logged_in()) {
		redirect("/login/");
	}
	
	app_include("authentication");
	
	if(user_is_admin() && segments_id() !== NULL) {
		$current_user = user_get_by_id(segments_id());
	} else {
		$current_user = user_get_by_id(user_id());
	}
	
	if(segments_action() == "delete") {
		user_delete_by_id($current_user['id']);
		if($current_user['id'] == user_id()) {
			redirect(segments_full() . "/logout=true");
		}
	}
	
	if(form_posted("edit_user")) {
		$update_array = array(
			"username" => form_validate("username"),
			"display_name" => form_validate("display_name"),
			"email" => form_validate("email"),
		);
		$password = form_validate("password");
		$conf_pass = form_validate("conf_pass");
		
		if($password) {
			if($password == $conf_pass) {
				if(user_set_password($current_user['id'], $password)) {
					notice_add("success", "Password was successfully changed.");
				} else {
					notice_add("error", "Error updating Password.");
				}
			} else {
				notice_add("error", "Passwords do not match, please try again.");
			}
		} else {
			notice_add("error", "Password was not updated");
		}
		if(user_update_by_id($current_user['id'], $update_array)) {
			notice_add("success", "Your changes were successfully saved.");
			
			// Refresh with latest info
			$current_user = user_get_by_id($current_user['id'], TRUE);
		} else {
			notice_add("error", "There was an error saving your changes.");
		}
	}
?>
<? layout_open("default"); ?>
	<? layout_section("content"); ?>
		<h1>Edit Your User</h1>
		<?=notices_show()?>
		<?=form_open("edit_user")?>
			<?=form_textbox("username", "Username: ", $current_user['username'])?>
			<?=form_textbox("display_name", "Full Name: ", $current_user['display_name'])?>
			<?=form_textbox("email", "Email: ", $current_user['email'])?>			
			<?=form_password("password", "New Password: (Leave blank for no change)")?>
			<?=form_password("conf_pass", "Confirm New Password: ")?>
			<?=form_button("submit", "", "Save Changes", array("control_class" => "large_button button_blue"))?>
			<a href="/users/delete/" class="" onclick="return confirm('Are you sure you want to delete your user information? There is NO UNDO')">Delete my account</a>
			<div class="clearboth"></div>
		<?=form_close()?>
	<? layout_section_close(); ?>
<? layout_close(); ?>
