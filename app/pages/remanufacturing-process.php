<?
meta("title", "Remanufacturing Process | Northwest Diesel Parts");
// meta description |.....................................................................................................................................................| 150 characters max
meta("description", "Learn more about the way we do things at Northwest Diesel Parts.");

$videos = array(Video::find(24), Video::find(25), Video::find(26), Video::find(27));
?>
<? layout_open("default"); ?>
<? layout_section("page-title"); ?>
    <h1>Remanufacturing Process</h1>
    <p>
        See how our remanufacting process works!
    </p>
<? layout_section_close(); ?>
<? layout_section("sidebar"); ?>
    <h5>Visit More Pages</h5>
    <ul class="submenu">
        <li><a href="/find-your-part/">Find Your Part</a></li>
        <li><a href="/remanufactured-long-block-engines/">Long Block Remanufacturing</a></li>
        <li><a href="/about/">About Us</a></li>
        <li><a href="/contact/">Contact Us</a></li>
    </ul>
<? layout_section_close(); ?>
<? layout_section("content"); ?>
<? $i = 0;
foreach ($videos as $k => $v):
    $i++;
    ?>
    <div class="float-left video-container">
        <a href="#?w=640" rel="video-<?= $v->id ?>" class="expand-video">
            <img src="http://img.youtube.com/vi/<?= $v->video_hash ?>/<?= $v->thumbnail_id ?>.jpg" width="290"/><img
                src="<?= APP_URL ?>/media/images/blue-transparent.png" class="play-button"/><br/>
            <span class="video-title"><?= $v->title ?></span>
        </a>
        <div class="popup_block" id="video-<?= $v->id ?>">
            <object style="height: 390px; width: 640px">
                <param name="movie" value="http://www.youtube.com/v/<?= $v->video_hash ?>">
                <param name="allowFullScreen" value="true">
                <param name="allowScriptAccess" value="always">
                <embed src="http://www.youtube.com/v/<?= $v->video_hash ?>" type="application/x-shockwave-flash"
                       allowfullscreen="true" allowScriptAccess="always" width="640" height="360"></embed>
            </object>
        </div>
    </div>
    <? if ($i % 2 == 0) { ?>
    <div class="cleared"></div>
<?php }
endforeach; ?>
    <div class="cleared"></div>
    <a href="/find-your-part/" class="button green-button">View Our Parts Catalog</a>
<? layout_section_close(); ?>
<? layout_close(); ?>