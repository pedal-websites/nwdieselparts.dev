<?
load_library("form");

meta("title", "Remanufactured Diesel Engine Components | Northwest Diesel Parts");
// meta description |.....................................................................................................................................................| 150 characters max
meta("description",
    "Northwest Diesel Parts offers remanufactured long block engines and many other new diesel engine components.  Get a free quote!");

//$parent_categories = Category::find_all_top_level();
?>
<? layout_open("home"); ?>
<? layout_section("full"); ?>
    <div id="home-page">
        <div id="attention">
            <span class="medium">Get your</span><br/>
            <span class="large">Diesel Parts Quote</span><br/>
            <span class="medium">Right now!</span><br/>
            <span class="small">During regular business hours, 7:30-4:30 PST. </span>
        </div>
        <div id="blue-headline">
            3 Easy Ways<br/>
            <span>to find your parts</span>
        </div>
        <div id="full-content">
            <div style="overflow: hidden">
                <div class="third">
                    <div class="easy-way">
                        <span class="number">1.</span>
                        <span class="way">Use our search feature</span>
                    </div>
                    <?= form_open("", "/find-your-part/", "get") ?>
                    <div class="search-wrapper">
                        <?= form_textbox("search", "", $search,
                            array("control_class" => "search-input", "placeholder" => "Search, then press enter")) ?>
                        <?= form_submit('submit-search', '', 'Search', array("control_class" => "search-submit")) ?>
                    </div>
                    <p>Search by description, part #, or casting #</p>
                    <p class="help">Example: "Cat 3306 Long Block", "PK983221"</p>
                    <?= form_close() ?>
                </div>
                <div class="third">
                    <div class="easy-way">
                        <span class="number">2.</span>
                        <span class="way">Browse our categories</span>
                    </div>
                    <p><a href="/find-your-part/" class="red">View our list of part categories</a></p>
                    <p><a href="/find-your-part/monthly-specials" style="color:#D32000;">Monthly Specials</a></p>
                    <? /*
						<ul class="home-submenu submenu">
							<? if(is_array($parent_categories)): ?>
								<? foreach($parent_categories as $c): ?>
									<li><a href="/find-your-part/<?=$c->slug?>/"><?=$c->name?></a></li>
								<? endforeach; ?>
							<? endif; ?>
						</ul>
						*/ ?>
                </div>
                <div class="third last">
                    <div class="easy-way">
                        <span class="number">3.</span>
                        <span class="way">Talk to our experts</span>
                    </div>
                    <div class="phone"><?= PHONE ?></div>
                    <div class="phone2">+<?= SECONDARY_PHONE ?></div>
                    <p class="centered"><a href="mailto:help@nwdieselparts.com">help@nwdieselparts.com</a></p>
                </div>
            </div>
            <div>
                <div class="infobox">
                    <img style="width:50%;" src="<?= APP_URL ?>/media/images/John-Deere-kit1.jpg" class="fl"/>
                    <h4><a href="<?= APP_URL ?>/remanufacturing-process/">Videos</a></h4>
                    <p>Check out these Northwest Diesel Parts reccomended videos.</p>
                    <a href="<?= APP_URL ?>/remanufacturing-process/" class="blue-button">View More</a>
                </div>
                <div class="infobox last">
                    <img src="<?= APP_URL ?>/media/images/group.jpg" height="161" class="fl"/>
                    <h4><a href="/remanufactured-long-block-engines/" style="font-size: 18px">Long Block
                            Manufacturing</a></h4>
                    <p>We offer a variety of remanufactured diesel long blocks and components for many industries.</p>
                    <a href="/remanufactured-long-block-engines/" class="blue-button">Read More</a>
                </div>
            </div>
        </div>
    </div>
<? layout_section_close(); ?>
<? layout_close(); ?>