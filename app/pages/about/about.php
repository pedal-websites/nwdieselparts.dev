<?
	meta("title", "About Us | Northwest Diesel Parts");
	// meta description |.....................................................................................................................................................| 150 characters max
	meta("description", "Learn more about Northwest Diesel Parts.  We're a family owned business with 60 years of experience in the heavy equipment industry.");
?>
<? layout_open("default"); ?>
	<? layout_section("page-title"); ?>
		<h1>About Us</h1>
		<p>
			We have the largest inventory of parts in the Northwest. If you can't find the part you're looking for, ask us! Or, request a quote for a part we have in stock.
		</p>
	<? layout_section_close(); ?>
	<? layout_section("sidebar"); ?>
		<h5>Visit More Pages</h5>
		<ul class="submenu">
			<li><a href="/find-your-part/">Find Your Part</a></li>
			<li><a href="/remanufactured-long-block-engines/">Long Block Remanufacturing</a></li>
			<li><a href="/about/">About Us</a></li>
			<li><a href="/contact/">Contact Us</a></li>
		</ul>
	<? layout_section_close(); ?>
	<? layout_section("content"); ?>
		<p>Northwest Casting Service is a family-owned business that works hard to ensure every product conforms to the strictest quality control standards. We employ a trained sales force in the field to meet your service and delivery needs. Feel free to <a href="/contact/">contact us</a> with any questions. We're convinced you'll be impressed by our competitive pricing, high-quality components, and superior customer service.</p>
		<p>We have assembled a team with over 60 years experience in the industry. Our thorough knowledge of diesel engine components can save you time and money by getting you the right product for your specific application. We strive to remain current with our knowledge while practicing the techniques that have proven themselves over time.</p>
		<p><strong>Why We Don’t Have Prices Online</strong></p>
		<p>There are many variables that determine the correct parts for your engine, Our sales staff have 25+ years experience on average in the diesel engine parts field, Let our experts determine the correct parts for your needs and find the lowest cost and highest quality parts to get your engine up and running as soon as possible!</p>
		<a href="/find-your-part/" class="button green-button">View Our Parts Catalog</a>
	<? layout_section_close(); ?>
<? layout_close(); ?>