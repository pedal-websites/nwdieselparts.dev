<?php
	$quote_submitted = (input_get("quote_submitted") == "true") ? TRUE : FALSE; // useful toggle for the interface

	//-----
	//- Create or Find current cart
	//-----
	if(cookie_isset("cart_id")) {
		$cart_id = cookie("cart_id");
	} else {
		$new_cart = new QuoteRequest();
		$new_cart->status = "cart";
		$new_cart->save();
		
		$cart_id = $new_cart->id;
		cookie("cart_id", $cart_id);
	}
	
	$cart = QuoteRequest::find_by_id($cart_id);
	
	//-----
	//- Delete a cart->item relationship -- NOT the part itself
	//-----
	if((segments(1) == "delete") && ((int)segments(0) > 0)) {
		$cart_part = QuoteRequestPart::find_by_id((int)segments(0));
		if($cart_part->delete()) {
			notice_add("success", "The part (" . $cart_part->part->number . ") was successfully removed from your quote.");
		} else {
			notice_add("error", "Error. Couldn't remove part.");
		}
		redirect("/cart/");
	}	
	
	//-----
	//- Form Handling
	//-----
	
	// Add item to cart
	if(form_posted("add_to_cart")) {
		$answers = form_value("answers");
		$question_answer_pairs = array();
		
		foreach($answers as $q_id => $answer) {
			$q = Question::find_by_id($q_id);
			$question_answer_pairs[] = array(
				"question" => $q->title,
				"answer" => $answer,
			);
		}
		
		$cart_part = new QuoteRequestPart();
		$cart_part->quote_request_id = $cart->id;
		$cart_part->part_id = form_value("part_id");
		$cart_part->question_answers = json_encode($question_answer_pairs);
		if($cart_part->save()) {
			// if((string)$_SERVER['REMOTE_ADDR'] == "50.240.17.106") die_print_pre($cart_part->to_json());
			notice_add("success", "The part (" . $cart_part->part->number . ") was successfully added to your quote.");
		} else {
			notice_add("error", "Something went wrong and the part may not have been added to your quote.");
		}
		
		redirect("/cart/");
	}
	
	// Submit Cart for Quote Request
	if(form_posted("submit_cart")) {
		$quote_request = QuoteRequest::find_by_id(cookie("cart_id"));
		$quote_request->status = "Submitted";
		$quote_request->submited_date = date(DATE_ATOM);
		$quote_request->name = form_validate("name", "trim|val_required");
		$quote_request->email = form_validate("email", "trim|val_required|val_email");
		$quote_request->phone = form_validate("phone", "trim|val_required");
		$quote_request->feedback = form_validate("feedback", "trim|val_required");
		$quote_request->referrer = cookie("VISITOR_REFERRER");
		
		if(form_is_valid()) {
			if($quote_request->save()) {
				$quote_request->notify();
				$person = array(
					"name" => form_value("name"),					
					"email" => form_value("email"),
					"phone" => form_value("phone"),					
					"feedback" => form_value("feedback"),					
				);
				cookie("person", base64_encode(serialize($person)));
				$quote_request_parts = $quote_request->quote_request_parts;
				// BEGIN EMAIL ***
					$subject = "Thank you for requesting a quote from " . settings("site", "name");
					$message = "<p>Thank you! Your quote request was sent successfully. We will get back to you as soon as possible. Here's a summary of your quote request:</p>";
					$message .= "<h3>Requested Parts:</h3>";
						
						foreach($quote_request_parts as $qrp) {
							$part = Part::find($qrp->part_id);
							$message .= "<p style='border:solid 1px black; padding:10px;'>Number: <a href='" . BASE_URL . "/find-your-part/" . $part->link . $part->slug . "/' target='_blank'><strong>{$part->number}</strong></a>";
							$message .= "<br />Description: {$part->description}<br />";
							$questions = json_decode($qrp->question_answers);
							if(is_array($questions)) {
								foreach($questions as $q) {
									$message .= $q->question . ": " . $q->answer . "<br />";
								}
							}
							$message .= "</p>";
						}
					$message .= "<h3>Feedback:</h3><p>{$quote_request->feedback}</p>";
					$message .= "<p>If you have any questions, call us: <strong>" . PHONE . "</strong>. Do not reply to this email, it is a summary of your quote request. You will be sent a quote in a separate email.</p>";
					$message .= "<p><a href='" . BASE_URL . "' target='_blank'>" . settings("site", "name") . "</a></p>";
					$to = $person['email'];
					$from =  EMAIL; // "<noreply@nwdieselparts.com>";
					$options = array();
					// $options['cc'] = EMAIL;
					load_library("email"); 
					phpmailer_send($to, $subject, $message, $from, $options);
				// END EMAIL ***
				notice_add("success", "You successfully submitted your quote request! Check your email to see a quote summary.");
				unset_cookie("cart_id");
			} else {
				notice_add("error", "Quote request not sent. There was an error. Please give us a call if this keeps happening.");
			}
		} else {
			notice_add("error", "Quote request not sent. Please make sure you filled out all of the fields.");			
		}
		redirect("/cart/?quote_submitted=true");		
	}
	
	//-----
	//- Data Handling
	//-----
	$cart_request_parts = $cart->quote_request_parts;
	$person = unserialize(base64_decode(cookie("person")));

?>
<? layout_open("default"); ?>
	<? layout_section("sidebar"); ?>
		<h2>Quote</h2>
		<div class="sidebar-buttons">
			<a href="#?w=500" rel="cart_questions" class="poplight green-button">Send Quote Request</a>
			<a href="/find-your-part/" class="blue-button">Find more Parts</a>
			<p>
				<strong>Sales tax:</strong>
				<em>We are located in the state of Oregon which has no sales tax, thus we do not collect sales tax on any purchase you may make from us.
				Please note that in some states the purchaser may need to pay "Use Tax" or a similar post-purchase tax 
				on parts. The purchaser is to comply with all state and local taxes that may apply.</em>
			</p>
		</div>
	<? layout_section_close(); ?>
	<? layout_section("content"); ?>
		<? if(!$quote_submitted) notice_add("info", "This is your cart for all of the parts you want quotes on.  Click 'Send Quote Request' to get your quote once you've added everything you need. <strong>We'll get back to you quickly.</strong>"); ?>
		<?=notices_show();?>
		
		<? if($quote_submitted): ?>
			<? // display custom information after a quote is submitted ?>
			
			<? // Social buttons ?>
			<p>Did you enjoy using our website?  If so, help us out by sharing it with your friends!</p>
			<!-- AddThis Button BEGIN -->
			<div class="addthis_toolbox addthis_default_style ">
				<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
				<a class="addthis_button_tweet"></a>
				<a class="addthis_counter addthis_pill_style"></a>
			</div>
			<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4e274e8827207c1b"></script>
			<!-- AddThis Button END -->
			
			<p>Please come back again!  If you'd like to start another quote, go ahead and visit the <a href="/find-your-part">find your part</a> page and add some more parts!</p>
			
		<? endif;?>

		
		
		<? if(count($cart_request_parts) > 0): ?>
			<h3>Quote (<?=count($cart_request_parts)?> items)</h3>			
			
			<? foreach($cart_request_parts as $qrp): ?>
				<? $part = Part::find_by_id($qrp->part_id); ?>
				<? if($part): ?>
					<div class="cart-part cleared">
						<? if(strlen($part->image_thumbname) > 1): ?>
							<a href="<?=BASE_URL?><?=$part->image_mediumname?>" target="_blank" class="lightbox" rel="lightbox-cart" title="<?=$part->number?>">
								<img src="<?=BASE_URL?><?=$part->image_thumbname?>" />Click to see full-sized photo
							</a>
						<? else: ?>
							<img src="<?=APP_URL?>/media/images/no-image.jpg" class="noimg" />
						<? endif; ?>
						<div class="cart-part-info">
							<p class="fr">
								<a href="/cart/<?=$qrp->id?>/delete/" class="confirm remove" data-confirm="Are you sure you want to remove '<?=$part->number?>' from your cart?">Remove from quote</a>
							</p>
							<p>
								Number: <a href="/find-your-part/<?=$part->link?><?=$part->slug?>/" target="_blank"><strong><?=$part->number?></strong></a><br />
								Alternate Numbers/Casting Numbers: <strong><?=$part->alternate_numbers?></strong>
							</p>
							<hr />
							<p><?=$part->description?></p><br />
							<hr />
							<? $questions = json_decode($qrp->question_answers); ?>
							<? if($questions): ?>
								<p>
									<? foreach($questions as $q): ?>
										<?=$q->question?>: <strong><?=$q->answer?></strong><br />
									<? endforeach; ?>
								</p>
							<? endif; ?>
						</div>
					</div>
				<? else: ?>
					<p>There was an error with this part. It may not be available anymore.</p>
				<? endif; ?>
			<? endforeach; ?>
			<a href="#?w=500" rel="cart_questions" class="green-button fr poplight">Request Quote On These Parts</a>
			<a href="/find-your-part/" class="blue-button fr" title="We'll save the parts in your quote already while you go find more">Find more Parts</a>
		<? else: ?>
			<? if(!$quote_submitted): ?>
				<p class="red">You have no items in your quote request. <a href="/find-your-part/">Find your part</a> now!</p>
			<? endif; ?>
			
		<? endif; ?>
		<div id="cart_questions" class="popup_block">
			<div class="popup_content">
				<p>Please fill out your contact info so that we can send you your quote.</p>
				<?=form_open("submit_cart", "/cart/")?>
					<?=form_textbox("name", "Name", $person['name'])?>
					<?=form_textbox("email", "Email", $person['email'])?>
					<?=form_textbox("phone", "Phone", $person['phone'])?>
					<?=form_textbox("feedback", "How did you find us?", $person['feedback'])?>
					<div class="cleared"></div>
					<?=form_button("submit", "", "Submit", array("control_class" => "button green-button", "class" => "fr")); ?>
				<?=form_close()?>
			</div>
		</div>
	<? layout_section_close(); ?>
<? layout_close(); ?>