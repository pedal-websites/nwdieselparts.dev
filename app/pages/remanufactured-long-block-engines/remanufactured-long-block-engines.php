<?
	meta("title", "About Our Remanufactured Long Blocks | NorthWest Diesel Parts");
	meta("description", "Learn more about our remanufactured long blocks, as well as other parts we sell.  Get a free quote!");
?>
<? app_include("functions"); ?>
<? layout_open("default"); ?>
	<? layout_section("page-title"); ?>
		<h1>Remanufactured Long Blocks</h1>
		<p>
			If time is of the essence (and isn't it always?), we have designed a Long Block Program
			that will get you up and running as quickly as possible. Our Long Blocks consist of an
			engine block, crankshaft, connecting rods, cylinder head and camshaft, loaded with new
			piston/sleeve kits, rod and main bearings and head gasket. We build our Long Blocks to
			your serial and arrangement number in order to meet your exact needs. And because
			we remanufacture our own components, we are able to strictly control the quality of your
			finished product. We offer superior packaging to protect your Long Block from shipping
			damage. Our crates also meet all requirements for export, so we can ship directly to
			your door from our facility.
		</p>	
	<? layout_section_close(); ?>
	<? layout_section("sidebar"); ?>
		<h5>Brands We Remanufacture</h5>
		<p>At our state-of-the-art facilities in Oregon, we remanufacture and rebuild diesel engine components from the world's leading manufacturers, including:</p>
		<ul>
			<li>Caterpillar</li>
			<li>Cummins</li>
			<li>Detroit Diesel</li>
		</ul>
		<p>Have a question? We're ready to help. <strong><?=PHONE?></strong>&nbsp;or<br /><strong><?=SECONDARY_PHONE?></strong></p>
		
	<? layout_section_close(); ?>
	<? layout_section("content"); ?>
	
		<? image("right", 300, "remanufactured-long-block-engine2", "3304 DI Long Block Engine", "Click for full size"); ?>
		<h5>Engine Components for Many Industries</h5>
		<p>We offer a variety of remanufactured diesel engine components for different industries, including:</p>
		<? app_include("industries-list"); ?>
		<p>Not only that, but we also offer many other diesel engine parts, so you can get all your parts from the same place.  Other parts we offer include connecting rods, water pumps, oil pumps and coolers, turbos, gasket kits, injectors, and more!</p>
		<!--<ul>
			<li>Fuel Pumps</li>
			<li>Water Pumps</li>
			<li>Oil Pumps</li>
			<li>Oil Coolers</li>
			<li>Turbos</li>
			<li>Gasket Kits</li>
			<li>Injectors</li>
		</ul>-->
		<h5>If you need some parts, go check out our parts catalog and <a href="/find-your-part/">find your parts</a>!</h5>
		<a href="/find-your-part/" class="green-button">View Our Parts Catalog</a>
		<a href="/about/" class="grey-button">Read More About Us</a>

	
	<? layout_section_close(); ?>	
<? layout_close(); ?>