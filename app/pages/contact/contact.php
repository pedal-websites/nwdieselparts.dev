<?
	load_library("form");
	if(form_posted("contact", false)) {
		$name = form_validate("name", "trim|val_required");
		$email = form_validate("email", "trim|val_required");
		$phone = form_validate("phone", "trim");
		$message_content = form_validate("message", "trim|val_required");
		$antispam = form_validate("antispam", "trim|val_required");
		
		if(form_is_valid()) {
			$valid = true;
			if(strtolower($antispam) == "ten" || (int)$antispam == 10) {
				$valid = true;
			} else {
				$valid = false;
			}
			$to = EMAIL;
			$subject = "Email: from " . $name;
				$message = "<strong>Message content:</strong><br />" . nl2br($message_content);
				$message .= "<br /><br />Name: " . $name . "<br />Email: " . $email . "<br />Phone: " . $phone;
				$message .= "<br /><br /><span style='color:#979797'>This message was sent using the contact form on " . BASE_URL . "</span>";
			$from = array(
				$name, 
				$email
			);
			// $from = "no-reply@nwdieselparts.com";
			$options = array();
			if($valid) {
				load_library("email"); 
				if(phpmailer_send($to, $subject, $message, $from, $options)){
					redirect("/contact/thank-you/");
				} else {
					echo phpmailer_send($to, $subject, $message, $from, $options);
					// redirect("/contact/thank-you/?error=couldn't send");
				}
			}
		} else {
			$valid = false;
		}
	}

// meta description |.....................................................................................................................................................| 150 characters max
	meta("description", "Contact NW Diesel Parts. We have a large inventory of long block parts.");
?>
<? layout_open("default"); ?>
	<? layout_section("page-title"); ?>
		<h1>Contact Us</h1>
	<p>Northwest Casting Service is a family-owned business that works hard to ensure every product conforms to the strictest quality control standards. We employ a trained sales force in the field to meet your service and delivery needs. Feel free to <a href="/contact/">contact us</a> with any questions. We're convinced you'll be impressed by our competitive pricing, high-quality components, and superior customer service.</p>
	<div id="sidebar">
		<h5>Visit More Pages</h5>
		<ul class="submenu">
			<li><a href="/find-your-part/">Find Your Part</a></li>
			<li><a href="/remanufactured-long-block-engines/">Long Block Remanufacturing</a></li>
			<li><a href="/about/">About Us</a></li>
			<li><a href="/contact/">Contact Us</a></li>
		</ul>
	</div>
	<p>We have assembled a team with over 60 years experience in the industry. Our thorough knowledge of diesel engine components can save you time and money by getting you the right product for your specific application. We strive to remain current with our knowledge while practicing the techniques that have proven themselves over time.</p>
	<p><strong>Why We Don’t Have Prices Online</strong></p>
	<p>There are many variables that determine the correct parts for your engine, Our sales staff have 25+ years experience on average in the diesel engine parts field, Let our experts determine the correct parts for your needs and find the lowest cost and highest quality parts to get your engine up and running as soon as possible!</p>	<? layout_section_close(); ?>
	<? layout_section("sidebar"); ?>
		<h2>Contact Info</h2>
		<div class="contact-info">
			<p><strong>Phone:</strong><br /><?=PHONE?><br /  /><?=SECONDARY_PHONE?></p>
			<p><strong>Email:</strong><br /><a href="mailto:help@nwdieselparts.com">help@nwdieselparts.com</a></p>
		</div>
	<? layout_section_close(); ?>
	<? layout_section("content"); ?>
		<? if($valid === false): ?>
			<p class="error-message">There was a problem with submittal. Please make sure you filled out the required fields. </p>
		<? elseif(segment(0) == "thank-you"): ?>
			<p class="success-message">Thanks for contacting us. We will respond to your email as soon as we can.</p>
		<? endif; ?>
		<? if(segment(0) !== "thank-you"): ?>
			<?=form_open("contact", "", "post", array("class" => "form-basic"))?>
				<div class="half">
					<?=form_textbox("name", "Name", form_value("name"), array("required" => "This field is required."))?>
					<?=form_textbox("email", "Email", form_value("email"), array("required" => "This field is required."))?>
					<?=form_textbox("phone", "Phone", form_value("phone"))?>
				</div>
				<div class="half">
					<?=form_textarea("message", "Message", form_value("message"), array("required" => "This field is required."))?>
					<?=form_textbox("antispam", "Antispam question: What is 6+4?", form_value("antispam"), array("required" => "This field is required."))?>
					<div class="cleared"></div>
					<?=form_submit("send-email", "", "Send Email", array("class" => "fr"))?>
				</div>
			<?=form_close()?>
		<? else: ?>
			<p>Return to the <a href="/contact/">contact form</a>.</p>
		<? endif; ?>
	<? layout_section_close(); ?>
<? layout_close(); ?>