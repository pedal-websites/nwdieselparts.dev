<?
	meta("title", "Videos | Northwest Diesel Parts");
	// meta description |.....................................................................................................................................................| 150 characters max
	meta("description", "Learn more about Northwest Diesel Parts.  We're a family owned business with 60 years of experience in the heavy equipment industry.");
	
	$videos = Video::all();
?>
<? layout_open("default"); ?>
	<? layout_section("page-title"); ?>
		<h1>Videos</h1>
		<p>
			We have the largest inventory of parts in the Northwest. If you can't find the part you're looking for, ask us! Or, request a quote for a part we have in stock.
		</p>
	<? layout_section_close(); ?>
	<? layout_section("sidebar"); ?>
		<h5>Visit More Pages</h5>
		<ul class="submenu">
			<li><a href="/find-your-part/">Find Your Part</a></li>
			<li><a href="/remanufactured-long-block-engines/">Long Block Remanufacturing</a></li>
			<li><a href="/about/">About Us</a></li>
			<li><a href="/contact/">Contact Us</a></li>
		</ul>
	<? layout_section_close(); ?>
	<? layout_section("content"); ?>
		<? foreach($videos as $k => $v): ?>
			<div class="float-left video-container">
				<a href="#?w=640" rel="video-<?=$v->id?>" class="expand-video">
					<img src="http://img.youtube.com/vi/<?=$v->video_hash?>/<?=$v->thumbnail_id?>.jpg" width="290"/><img src="<?=APP_URL?>/media/images/blue-transparent.png" class="play-button" /><br />
					<span class="video-title"><?=$v->title?></span>
				</a>
				<div class="popup_block" id="video-<?=$v->id?>">
					<object style="height: 390px; width: 640px"><param name="movie" value="http://www.youtube.com/v/<?=$v->video_hash?>"><param name="allowFullScreen" value="true"><param name="allowScriptAccess" value="always"><embed src="http://www.youtube.com/v/<?=$v->video_hash?>" type="application/x-shockwave-flash" allowfullscreen="true" allowScriptAccess="always" width="640" height="360"></embed></object>
				</div>
			</div>
		<? endforeach; ?>
		<div class="cleared"></div>
		<a href="/find-your-part/" class="button green-button">View Our Parts Catalog</a>
	<? layout_section_close(); ?>
<? layout_close(); ?>